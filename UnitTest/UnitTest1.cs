﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;
using Gateway.Controllers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Common.PayDash;
using Gateway;
using System.Threading;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void RedisTestMethod()
        {
            using (var db = new cellEntities())
            {
                RedisClient c = new RedisClient();
                c.SaveDLRXml("test");
                c.SaveDLRXml("test1");
                c.SaveDLRXml("test2");
                while (true)
                {
                    var s = c.GetNextDLRXml();
                    if (s != null)
                        ThreadPool.QueueUserWorkItem(TaskCallBack, s);
                    else
                        Thread.Sleep(100);
                }
            }
        }

        private static void
            TaskCallBack(Object dlr)
        {
            
            Console.WriteLine(dlr.ToString()
                + "  Finished...");
        }

        [TestMethod]
        public void TestMethod1()
        {
            using (var db = new cellEntities())
            {
                var dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var MONTHLY_SUM = 2.08m * 4 * 3;
                var users = db.Users.Where(x => x.Status == (int)UserStatus.subscribed);
                for (int i = 0; i < users.Count() ; i = i + 100)
                {
                    var items = users.OrderBy(x=>x.CreationDate).Skip(i).Take(100);
                    foreach (var u in items)
                    {
                        try
                        {
                            var trans = u.UserTransactions.Where(a => (a.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || a.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && a.State == Utility.DELIVRD && a.TransactionDate > dt);
                            var sum = trans.Sum(a => a.Tariff);
                            if (sum == MONTHLY_SUM)
                            {
                                u.Status = (int)UserStatus.active;
                            }
                            if (sum < MONTHLY_SUM)
                            {
                                u.Status = (int)UserStatus.semibilled;
                            }

                            if (u.JoinMethod == 4)
                            {
                                u.Status = (int)UserStatus.active;
                            }
                        }
                        catch(Exception ex)
                        {

                        }
                    }
                    db.SaveChanges();

                }
                
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            var srv = new SMSEngine.Service();
            //srv.DLRWorkerThreadFunc();
            srv.ProcessMO("<?xml version=\"1.0\" standalone=\"no\"?><!DOCTYPE TPBOUND_MESSAGES SYSTEM \"tpbound_messages_v1.dtd\"><TPBOUND_MESSAGES><SMSTOTP><SOURCE_ADDR>306936170757</SOURCE_ADDR><TEXT>OK</TEXT><GWTRANSACTIONID>f34035d7-e1bf-4b42-8388-079e05c2fa7d</GWTRANSACTIONID><DESTINATION_ADDR>54303</DESTINATION_ADDR><SERVICEID>1</SERVICEID><NETWORKID>635</NETWORKID><ARRIVALDATETIME><DD>27</DD><MMM>MAR</MMM><YYYY>2020</YYYY><HH>11</HH><MM>17</MM></ARRIVALDATETIME></SMSTOTP></TPBOUND_MESSAGES>");
            //RedisClient cli = new RedisClient();
            //var oo = cli.GetDLRListLen();

        }

        [TestMethod]
        public void TestMethod3()
        {
            PayDashClient p = new PayDashClient(1);
            var res = p.Lookup("6971868265");
        }

        [TestMethod]
        public void TestMethod4()
        {
            var Id = 271785;
            var db = new cellEntities();
            var newUser = db.Users.Where(uu => uu.Id == Id).FirstOrDefault();

            if ((newUser.JoinMethod == (int)UserJoinMethod.auto && newUser.UserTransactions.OrderByDescending(a => a.TransactionDate).First().State == Utility.DELIVRD) || (newUser.Status == (int)UserStatus.entered && AutoRegisterEngine.IsVaidAff(newUser) && AutoRegisterEngine.IsHasNoTransactions(newUser) && AutoRegisterEngine.NotFromKnownIP(newUser, db)))
            {
                var con = new UsersController();
                con.Request = new System.Net.Http.HttpRequestMessage();
                con.ValidateUserPinCode(newUser.PhoneNumber, newUser.PinCode, newUser.ServiceId);
                newUser.Service.AutoRegisterCount = newUser.Service.AutoRegisterCount + 1;
                db.SaveChanges();
                //logger.Debug("User id :{0} was autoregistered", newUser.Id);
            }


        }

        [TestMethod]
        public void TestMethod5()
        {
            var message = "Cell stop";
            List<string> stopList = new List<string>() { "stop","ΣTOP" ,"ΣΤOP","ΣΤΟP","ΣΤOΠ","ΣTΟP","ΣTΟΠ","ΣTOΠ"

,"SΤOP"
,"SΤΟP"
,"SΤOΠ"
,"SΤΟΠ"
,"STΟP"
,"STΟΠ"
,"STOΠ"
,"ΣΤΟΠ"
,"σtop"
,"στοp"
,"στοp"
,"στoπ"
,"σtοp"
,"σtοπ"
,"σtoπ"
,"sτop"
,"sτοp"
,"sτoπ"
,"sτοπ"
,"stοp"
,"stοπ"
,"stoπ"
,"στοπ" };

            Parallel.ForEach(stopList,(s) =>
            {
                Console.WriteLine(s);
            }
           );


        }
    }
}
