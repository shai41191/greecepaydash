﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DAL
    {
        public int CreateUser(User user)
        {
            SqlConnection conn = null;

            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["cellEntities"].ConnectionString;
                string[] str = connectionString.Split('"');

                SqlDataReader rdr = null;

                // create and open a connection object
                conn = new SqlConnection(str[1]);
                conn.Open();

                // 1. create a command object identifying
                // the stored procedure
                SqlCommand cmd = new SqlCommand(
                    "uspUserCreate", conn);
                
                cmd.Parameters.Add(new SqlParameter("@phoneNumber", user.PhoneNumber));
                cmd.Parameters.Add(new SqlParameter("@serviceid", user.ServiceId));
                cmd.Parameters.Add(new SqlParameter("@status", user.Status));
                cmd.Parameters.Add(new SqlParameter("@affiliateID", user.AffiliateID));
                cmd.Parameters.Add(new SqlParameter("@ip", user.IP));
                cmd.Parameters.Add(new SqlParameter("@pinCode", user.PinCode));
                cmd.Parameters.Add(new SqlParameter("@joinMethod", user.JoinMethod));
                cmd.Parameters.Add(new SqlParameter("@operator", user.Operator));
                cmd.Parameters.Add(new SqlParameter("@affiliateParams", user.AffiliateParams));
                cmd.Parameters.Add(new SqlParameter("@url", user.URL));

                cmd.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorCode", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 4000).Direction = ParameterDirection.Output;

                // 2. set the command object so it knows
                // to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // execute the command
                int i = cmd.ExecuteNonQuery();

                var ErrorCode = Convert.ToInt32(cmd.Parameters["@ErrorCode"].Value);
                if (ErrorCode == 0)
                {
                    var id = Convert.ToInt32(cmd.Parameters["@Id"].Value);
                    return id;
                }
                else
                {
                    return 0;
                }



            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }
        }

        public string GetNextTransactionId()
        {
            SqlConnection conn = null;

            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["cellEntities"].ConnectionString;
                string[] str = connectionString.Split('"');

                SqlDataReader rdr = null;

                // create and open a connection object
                conn = new SqlConnection(str[1]);
                conn.Open();

                string sql = @"SELECT NEXT VALUE FOR TransactionsId ";

                // 1. create a command object identifying
                // the stored procedure
                SqlCommand cmd = new SqlCommand(
                    sql, conn);

                string seq = cmd.ExecuteScalar().ToString();
                return seq;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }


        }
    }
}
