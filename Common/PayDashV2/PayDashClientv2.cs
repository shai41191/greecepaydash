﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDashV2
{
    public class PayDashClientv2
    {
        RestClient client;
        const string BASE_URL = "http://dc.paydash.gr:8001/sms";
        public const string KEYWORD = "keyword";
        public const string PASSWORD = "password";
        public string User { get; set; }
        public string Password { get; set; }

        public PayDashClientv2(int serviceid)
        {
            client = new RestClient(BASE_URL);
            User = ServicesProvider.GetString(serviceid, KEYWORD);
            Password = ServicesProvider.GetString(serviceid, PASSWORD);
        }

        public PayDashResponse SubscribeUser(string MSISDN,int PINId,int NetworkId)
        {
            MSISDN = AddCountryPrefix(MSISDN);
            var request = new RestRequest($"/subsms/{User}/{Password}/", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new {MSISDN,PINId,NetworkId });
            
            var response = Execute<PayDashResponse>(request);

            return response;
        }

        private static string AddCountryPrefix(string to)
        {
            to = "30" + to;
            return to;
        }

        public T Execute<T>(RestRequest request) where T : new()
        {
            var response = client.Execute(request);
            if (response.ErrorException != null)
            {
                const string message = "Error retrieving response.  Check inner details for more info.";
                var Exception = new ApplicationException(message, response.ErrorException);
                throw Exception;
            }
            if (response.StatusCode != System.Net.HttpStatusCode.OK && response.StatusCode != System.Net.HttpStatusCode.Created && response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                var Exception = new Exception($"http returned status code:{response.StatusCode}, StatusDescription:{ response.StatusDescription}, Content: {response.Content}");
                throw Exception;
            }

            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
