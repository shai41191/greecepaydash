﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDashV2
{
    public class MOReq
    {
        public string Sender { get; set; }
        public string Text { get; set; }
        public int NetworkId { get; set; }
        public string Shortcode { get; set; }
        public string DateTimeSent { get; set; }
    }
}
