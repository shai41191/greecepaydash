﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDashV2
{
    public class PayDashResponse
    {
        public string status { get; set; }
        public List<string> messages { get; set; }
        public dynamic payload { get; set; }
    }
}
