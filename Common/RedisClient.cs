﻿using Common.PayDash;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class RedisClient
    {
        private const string DLR_KEY = "DLR";
        private const string MO_KEY = "MO";

        public IDatabase db { get; set; }
        public RedisClient()
        {
            ConnectionMultiplexer muxer = ConnectionMultiplexer.Connect("redis-12617.c135.eu-central-1-1.ec2.cloud.redislabs.com:12617,password=VD2khIj66LXEaqLKiwbpWwJ9Dgjv8BN3");
            db = muxer.GetDatabase();
        }

        #region DLR
        public void SaveDLRXml(string dlr)
        {
            db.ListLeftPush(DLR_KEY, dlr);
        }

        public string GetNextDLRXml()
        {
            var value = db.ListRightPop(DLR_KEY);
            return value;
        }

        public long GetDLRListLen()
        {
            return db.ListLength(DLR_KEY);
        }

        #endregion

        #region MO
        public void SaveMOXml(string dlr)
        {
            db.ListLeftPush(MO_KEY, dlr);
        }

        public string GetNextMOXml()
        {
            var value = db.ListRightPop(MO_KEY);
            return value;
        }

        public long GetMOListLen()
        {
            return db.ListLength(MO_KEY);
        } 
        #endregion
    }
}
