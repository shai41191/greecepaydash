﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class ServicesProvider
    {
        public static List<Service> Services;
        static ServicesProvider()
        {
            using (var db = new cellEntities())
            {
                Services = db.Services.ToList();
            } 
        }

        public static String GetString(int serviceid, string str)
        {
            var s = Services.Where(service => service.Id == serviceid).First();
            return s.GetType().GetProperty(str).GetValue(s, null).ToString();
        }

        
    }
}
