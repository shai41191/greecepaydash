﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDash
{
    public class SubResponse : IPayDashRegBaseResponse
    {
        public Response response { get; set; }

        public class Response
        {
            public string status { get; set; }
            public string error { get; set; }
        }

        public string GetStatus()
        {
            return this.response.status;
        }

        public string GetError()
        {

            return this.response.error;
        }

        public override string ToString()
        {
            return string.Format("Status:{0},error:{1}", this.response.status, this.response.error);
        }
    }


}
