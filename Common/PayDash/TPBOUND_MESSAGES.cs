﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDash
{
    public class TPBOUND_MESSAGES
    {
        public string SOURCE_ADDR { get; set; }
        public string TEXT { get; set; }
        public string GWTRANSACTIONID { get; set; }
        public string DESTINATION_ADDR { get; set; }
        public int SERVICEID { get; set; }
        public string NETWORKID { get; set; }
    }
}
