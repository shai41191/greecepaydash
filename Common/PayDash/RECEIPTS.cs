﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDash
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class RECEIPTS
    {

        private RECEIPTSSMSRECEIPT sMSRECEIPTField;

        /// <remarks/>
        public RECEIPTSSMSRECEIPT SMSRECEIPT
        {
            get
            {
                return this.sMSRECEIPTField;
            }
            set
            {
                this.sMSRECEIPTField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RECEIPTSSMSRECEIPT
    {

        private int sERVICEIDField;

        private string sOURCE_ADDRField;

        private string tRANSACTIONIDField;

        private int nETWORKIDField;

        private int sTATUSIDField;

        private RECEIPTSSMSRECEIPTSTATUSDATETIME sTATUSDATETIMEField;

        private byte tOTALFRAGMENTNOField;

        private byte fRAGMENTIDField;

        /// <remarks/>
        public int SERVICEID
        {
            get
            {
                return this.sERVICEIDField;
            }
            set
            {
                this.sERVICEIDField = value;
            }
        }

        /// <remarks/>
        public string SOURCE_ADDR
        {
            get
            {
                return this.sOURCE_ADDRField;
            }
            set
            {
                this.sOURCE_ADDRField = value;
            }
        }

        /// <remarks/>
        public string TRANSACTIONID
        {
            get
            {
                return this.tRANSACTIONIDField;
            }
            set
            {
                this.tRANSACTIONIDField = value;
            }
        }

        /// <remarks/>
        public int NETWORKID
        {
            get
            {
                return this.nETWORKIDField;
            }
            set
            {
                this.nETWORKIDField = value;
            }
        }

        /// <remarks/>
        public int STATUSID
        {
            get
            {
                return this.sTATUSIDField;
            }
            set
            {
                this.sTATUSIDField = value;
            }
        }

        /// <remarks/>
        public RECEIPTSSMSRECEIPTSTATUSDATETIME STATUSDATETIME
        {
            get
            {
                return this.sTATUSDATETIMEField;
            }
            set
            {
                this.sTATUSDATETIMEField = value;
            }
        }

        /// <remarks/>
        public byte TOTALFRAGMENTNO
        {
            get
            {
                return this.tOTALFRAGMENTNOField;
            }
            set
            {
                this.tOTALFRAGMENTNOField = value;
            }
        }

        /// <remarks/>
        public byte FRAGMENTID
        {
            get
            {
                return this.fRAGMENTIDField;
            }
            set
            {
                this.fRAGMENTIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class RECEIPTSSMSRECEIPTSTATUSDATETIME
    {

        private int ddField;

        private string mMMField;

        private int yYYYField;

        private int hhField;

        private int mmField;

        /// <remarks/>
        public int DD
        {
            get
            {
                return this.ddField;
            }
            set
            {
                this.ddField = value;
            }
        }

        /// <remarks/>
        public string MMM
        {
            get
            {
                return this.mMMField;
            }
            set
            {
                this.mMMField = value;
            }
        }

        /// <remarks/>
        public int YYYY
        {
            get
            {
                return this.yYYYField;
            }
            set
            {
                this.yYYYField = value;
            }
        }

        /// <remarks/>
        public int HH
        {
            get
            {
                return this.hhField;
            }
            set
            {
                this.hhField = value;
            }
        }

        /// <remarks/>
        public int MM
        {
            get
            {
                return this.mmField;
            }
            set
            {
                this.mmField = value;
            }
        }
    }


}
