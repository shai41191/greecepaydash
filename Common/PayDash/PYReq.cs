﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDash
{
    public  class PYReq
    {
        public string USER { get; set; }
        public string Password { get; set; }
        public string TP_XML { get; set; }
    }
}
