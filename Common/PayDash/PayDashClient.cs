﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Common.PayDash
{
    public class PayDashClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public const string BASE_SEND_SMS_URL = "http://dc.paydash.gr:8002/hlr/v2/"; //http://web1.paydash.gr/amsms/";
        public WebClient webclient;
        public const string KEYWORD = "keyword";
        public const string PASSWORD = "password";
        public string WELCOME_MSG = "WelcomeMessage";
        public string PIN_CODE_MSG = "PincodeMessage";
        public string ALREADY_SUBSCRIBER_SMS_MSG = "AlreadySubscriberMessage";
        public string UNSUBSCRIBED_SMS_MSG = "UnsubscribedMessage";
        public string FIFTEEN_DAYS_UNBILLED_USERS_REMINDER_MSG = "FifteenDaysReminderMessage";
        public string MONTHLY_REMINDER_MSG = "MonthlyReminderMessage";
        public const string SRV = "AppIDHigh";
        public const string AppIDLow = "AppIDLow";

        public string User { get; set; }
        public string Password{ get; set; }


        public PayDashClient(int serviceid)
        {
            webclient = new WebClient();
            webclient.Encoding = Encoding.UTF8;
            webclient.BaseAddress = BASE_SEND_SMS_URL;
            User =  ServicesProvider.GetString(serviceid, KEYWORD);
            Password = ServicesProvider.GetString(serviceid, PASSWORD);
        }

        public string GetUsrId(string msisdn)
        {
            logger.Debug($"GetUsrId for: {msisdn}");
            msisdn = AddCountryPrefix(msisdn);
            webclient.QueryString.Add("msisdn", msisdn);
            LogQueryString("getusrid");
            dynamic obj = JObject.Parse(webclient.DownloadString("getusrid.ashx"));
            return obj.id;
        }

        public GenPinResponse GenPin(string uuid,int serviceid)
        {
            var srv = ServicesProvider.GetString(serviceid, SRV);
            logger.Debug($"GenPin for uuid: {uuid}");
            webclient.QueryString.Add("usr", uuid);
            webclient.QueryString.Add("srv", srv);
            //webclient.QueryString.Add("ntwrk", "265");
            LogQueryString("genpin");
            GenPinResponse obj = JsonConvert.DeserializeObject<GenPinResponse>(webclient.DownloadString("genpin.ashx"));
            return obj;
        }

        public SubResponse Sub(string pin, string id,string uuid, int serviceid)
        {
            logger.Debug($"Sub for pin: {pin}, id:{id} uuid: {uuid}");
            var srv = ServicesProvider.GetString(serviceid, SRV);
            webclient.QueryString.Add("usr", uuid);
            webclient.QueryString.Add("srv", srv);
            webclient.QueryString.Add("pin", pin);
            webclient.QueryString.Add("id", id);

            LogQueryString("sub");
            SubResponse obj = JsonConvert.DeserializeObject<SubResponse>(webclient.DownloadString("sub.ashx"));
            return obj;
        }

        public string SendPinCodeSMS(string to, string pinCode, string operatorCode, int serviceid, string seqid)
        {
            logger.Debug($"SendWelcomeSMS to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{ServicesProvider.GetString(serviceid, WELCOME_MSG)} ");
            return SendMsg(to, operatorCode, serviceid, seqid, String.Format(ServicesProvider.GetString(serviceid, PIN_CODE_MSG), pinCode), true);
        }

        public string SendWelcomeSMS(string to, string operatorCode, int serviceid,string seqid)
        {
            logger.Debug($"SendWelcomeSMS to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{ServicesProvider.GetString(serviceid, WELCOME_MSG)} ");
            return SendMsg(to, operatorCode, serviceid, seqid, ServicesProvider.GetString(serviceid, WELCOME_MSG) ,true);
        }

        public string Send15DaysUnbilledUsers(string to, string operatorCode, int serviceid, string seqid)
        {
            logger.Debug($"Send15DaysUnbilledUsers to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{ServicesProvider.GetString(serviceid, FIFTEEN_DAYS_UNBILLED_USERS_REMINDER_MSG)} ");
            return SendMsg(to, operatorCode, serviceid, seqid, ServicesProvider.GetString(serviceid,FIFTEEN_DAYS_UNBILLED_USERS_REMINDER_MSG), true);
        }

        public string SendMonthlyReminder(string to, string operatorCode, int serviceid, string seqid)
        {
            logger.Debug($"SendMonthlyReminder to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{ServicesProvider.GetString(serviceid, MONTHLY_REMINDER_MSG)} ");
            return SendMsg(to, operatorCode, serviceid, seqid, ServicesProvider.GetString(serviceid, MONTHLY_REMINDER_MSG), true);
        }


        public string SendPaidSMS(string to, string operatorCode, int serviceid, string seqid,string msg,bool isSmall=false)
        {
            logger.Debug($"SendPaidSMS to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{msg} ");
            return SendMsg(to, operatorCode, serviceid, seqid, msg,false, isSmall);
        }

        public string SendAlerdaySubscriberSMS(string to, string operatorCode, int serviceid, string seqid)
        {
            logger.Debug($"SendAlerdaySubscriberSMS to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{ServicesProvider.GetString(serviceid, ALREADY_SUBSCRIBER_SMS_MSG)} ");
            return SendMsg(to, operatorCode, serviceid, seqid, ServicesProvider.GetString(serviceid, ALREADY_SUBSCRIBER_SMS_MSG),true);
        }

        public string SendUnsubscriptionSMS(string to, string operatorCode, int serviceid, string seqid)
        {
            logger.Debug($"SendUnsubscriptionSMS to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},message:{ServicesProvider.GetString(serviceid, UNSUBSCRIBED_SMS_MSG)} ");
            return SendMsg(to, operatorCode, serviceid, seqid, ServicesProvider.GetString(serviceid, UNSUBSCRIBED_SMS_MSG), true);
        }

        private string SendMsg(string to, string operatorCode, int serviceid, string seqid,string msg , bool isfree, bool isSmall = false)
        {
            logger.Debug($"SendMsg to:{to},operatorCode:{operatorCode},serviceid:{serviceid},seqid:{seqid},isfree:{isfree},message:{msg}");
            var srv =int.Parse(ServicesProvider.GetString(serviceid, "ServiceId"));
            to = AddCountryPrefix(to);


            webclient.QueryString.Add("User", User);
            webclient.QueryString.Add("password", Password);
            webclient.QueryString.Add("GW_XML", HttpUtility.UrlEncode(GenrateMTXml(to, srv, msg, operatorCode, isfree, seqid,isSmall)));

            LogQueryString(msg);
            return webclient.DownloadString("outmt.ashx");
        }

        public LookupResult Lookup(string msisdn)
        {
            msisdn = AddCountryPrefix(msisdn);
            var client = new RestClient(BASE_SEND_SMS_URL+User+"/"+Password);
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new 
            {
                /* user = User,
                 password = Password,*/
                MSISDN = msisdn
            });
            var res = client.Execute<LookupResult>(request);
            return res.Data;
        }

        private static string AddCountryPrefix(string to)
        {
            to = "30" + to;
            return to;
        }

        private void LogQueryString(string action)
        {
            string str = action;
            foreach (String key in webclient.QueryString)
            {
                str += "&" + key + "=" + webclient.QueryString[key];
            }

            logger.Info(str);
        }

        public int OperatorToNetworkId(string Operator)
        {
            if (Operator.Equals("GR_COSMOTE", StringComparison.CurrentCultureIgnoreCase))
                return 265;
            if (Operator.Equals("GR_VODAFONE", StringComparison.CurrentCultureIgnoreCase))
                return 268;
            if (Operator.Equals("GR_WIND", StringComparison.CurrentCultureIgnoreCase))
                return 635;

            return 0;

        }

        public string GenrateMTXml(string to , int serviceid, string msg , string Operator , bool isFree , string tranId , bool isSmall = false)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(GW_DELIVERY_2_SMS));
            var subReq = new GW_DELIVERY_2_SMS();
            subReq.SMSMESSAGE = new GW_DELIVERY_2_SMSSMSMESSAGE();
            subReq.SMSMESSAGE.SERVICEID = serviceid;
            subReq.SMSMESSAGE.TEXT = msg;
            subReq.SMSMESSAGE.NETWORKID = OperatorToNetworkId(Operator);
            subReq.SMSMESSAGE.COSTID = 1; //default paid
            if (isFree)
            {
                //for costid 3 use srv 1
                subReq.SMSMESSAGE.SERVICEID = serviceid;
                subReq.SMSMESSAGE.COSTID = 3;
            }
            if (isSmall)
            {

                subReq.SMSMESSAGE.COSTID = Int32.Parse(ServicesProvider.GetString(serviceid, AppIDLow));//2;// if its 0.52 send 2
            }

            subReq.SMSMESSAGE.DELIVERYRECEIPT = 13;
            subReq.SMSMESSAGE.TRANSACTIONID = tranId;
            subReq.SMSMESSAGE.TYPEID = 2;
            subReq.SMSMESSAGE.DESTINATION_ADDR = to;
            

            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    writer.WriteDocType("GW_DELIVERY_2_SMS",null, "gwbound_messages_v1.dtd", null);
                    xsSubmit.Serialize(writer, subReq,ns);
                    xml = sww.ToString(); // Your XML
                }
            }
            //xml = xml.Replace("<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "<?xml version=\"1.0\" standalone=\"no\"?> < !DOCTYPE GW_DELIVERY_2_SMS SYSTEM \"gwbound_messages_v1.dtd\" > ");
            //logger.Debug(xml);
            return xml;
        }
    }
}
