﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDash
{
    public interface IPayDashRegBaseResponse
    {
        string GetStatus();
        string GetError();
    }
}
