﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.PayDash
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class GW_DELIVERY_2_SMS
    {

        private GW_DELIVERY_2_SMSSMSMESSAGE sMSMESSAGEField;

        /// <remarks/>
        public GW_DELIVERY_2_SMSSMSMESSAGE SMSMESSAGE
        {
            get
            {
                return this.sMSMESSAGEField;
            }
            set
            {
                this.sMSMESSAGEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class GW_DELIVERY_2_SMSSMSMESSAGE
    {

        private string dESTINATION_ADDRField;

        private string tEXTField;

        private string tRANSACTIONIDField;

        private int tYPEIDField;

        private int sERVICEIDField;

        private int cOSTIDField;

        private int nETWORKIDField;


        private int dELIVERYRECEIPTField;



        /// <remarks/>
        public string DESTINATION_ADDR
        {
            get
            {
                return this.dESTINATION_ADDRField;
            }
            set
            {
                this.dESTINATION_ADDRField = value;
            }
        }

        /// <remarks/>
        public string TEXT
        {
            get
            {
                return this.tEXTField;
            }
            set
            {
                this.tEXTField = value;
            }
        }

        /// <remarks/>
        public string TRANSACTIONID
        {
            get
            {
                return this.tRANSACTIONIDField;
            }
            set
            {
                this.tRANSACTIONIDField = value;
            }
        }

        /// <remarks/>
        public int TYPEID
        {
            get
            {
                return this.tYPEIDField;
            }
            set
            {
                this.tYPEIDField = value;
            }
        }

        /// <remarks/>
        public int SERVICEID
        {
            get
            {
                return this.sERVICEIDField;
            }
            set
            {
                this.sERVICEIDField = value;
            }
        }

        /// <remarks/>
        public int COSTID
        {
            get
            {
                return this.cOSTIDField;
            }
            set
            {
                this.cOSTIDField = value;
            }
        }



        /// <remarks/>
        public int NETWORKID
        {
            get
            {
                return this.nETWORKIDField;
            }
            set
            {
                this.nETWORKIDField = value;
            }
        }



        /// <remarks/>
        public int DELIVERYRECEIPT
        {
            get
            {
                return this.dELIVERYRECEIPTField;
            }
            set
            {
                this.dELIVERYRECEIPTField = value;
            }
        }

    }


}
