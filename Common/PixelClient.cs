﻿using Common;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using NLog;
using RestSharp;

namespace Common
{
    public class PixelClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void FireEnter(User user)
        {
            logger.Debug($"in pixel client FireEnter user id:{user.Id}");
            if(user.Affiliate != null && user.Affiliate.FireOnEnter.GetValueOrDefault())
            {
                logger.Debug($"SendAffilatePixel for FireEnter on user id:{user.Id}");
                SendAffilatePixel(user, false,"Enter");
            }
        }

        public static void FireSubscribed(User user)
        {
            logger.Debug($"in pixel client FireSubscribed user id:{user.Id}");
            if (user.Affiliate != null && user.Affiliate.FireOnSubscribed.GetValueOrDefault())
            {
                logger.Debug($"SendAffilatePixel for FireSubscribed user id:{user.Id}");
                SendAffilatePixel(user, false, "Subscribed");
            }
        }

        public static void FireBlacKllist(User user)
        {
            logger.Debug($"in pixel client FireSubscribed user id:{user.Id}");
            if (user.Affiliate != null && user.Affiliate.FireOnBlackllist.GetValueOrDefault())
            {
                logger.Debug($"SendAffilatePixel for FireSubscribed user id:{user.Id}");
                SendAffilatePixel(user, false, "Blacllist");
            }
        }


        //public static void SendAffilatePixel(User user, bool isLate,String action)
        //{
        //    logger.Debug($"SendAffilatePixel for user id:{user.Id}");
        //    //only one pixel for user
        //    if (user.AffiliateTransactions.Count() > 0)
        //    {
        //        logger.Debug($"allready sent pixel for user id:{user.Id}");
        //        return;
        //    }

        //    try
        //    {
        //        var aff = user.Affiliate;
        //        if (aff == null || user.JoinMethod == (int)UserJoinMethod.auto)
        //            return;
        //        var url = aff.Url;
        //        var parms = user.AffiliateParams;
        //        if (!string.IsNullOrEmpty(parms))
        //        {
        //            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(parms);
        //            var p = aff.param1;
        //            if (!string.IsNullOrEmpty(p))
        //            {

        //                if (values.ContainsKey(p))
        //                    url = url.Replace("[param1]", values[p]);
        //            }

        //            p = aff.param2;
        //            if (!string.IsNullOrEmpty(p))
        //            {
        //                if (values.ContainsKey(p))
        //                    url = url.Replace("[param2]", values[p]);
        //            }

        //            p = aff.param3;
        //            if (!string.IsNullOrEmpty(p))
        //            {
        //                if (values.ContainsKey(p))
        //                    url = url.Replace("[param3]", values[p]);
        //            }

        //            p = aff.param4;
        //            if (!string.IsNullOrEmpty(p))
        //            {
        //                if (values.ContainsKey(p))
        //                    url = url.Replace("[param4]", values[p]);
        //            }

        //            p = aff.param5;
        //            if (!string.IsNullOrEmpty(p))
        //            {
        //                if (values.ContainsKey(p))
        //                    url = url.Replace("[param5]", values[p]);
        //            }


        //        }
        //        logger.Debug("affilate url fired:" + url);
        //        var res = ((HttpWebRequest)WebRequest.Create(url)).GetResponse();
        //        AffiliateTransaction at = new AffiliateTransaction();
        //        at.AffiliateId = aff.Id;
        //        at.Requset = url;
        //        at.TransactionDate = DateTime.Now;
        //        at.UserId = user.Id;
        //        at.IsLatePixel = isLate;
        //        at.Reason = action;
        //        at.PayOut = aff.param5;
        //        at.ResultStatus = (((HttpWebResponse)res).StatusDescription);
        //        user.AffiliateTransactions.Add(at);

        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex);
        //    }
        //}

        public static bool SendUrl(string Url, int UserId)
        {
            logger.Debug($"SendToGlobal for user id:{UserId}");
            var Pixelclient = new RestClient("http://affnew-dev.us-west-2.elasticbeanstalk.com/Users/SendPixel?UserId=" + UserId);
            Pixelclient.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddParameter("data", Url);
            //request.AddParameter("UserId", UserId);
            var res = Pixelclient.Execute(request);
            return res.IsSuccessful;
        }



        



        public static void SendAffilatePixel(Common.User user, bool isLate, String action)
        {
            try
            {
                var aff = user.Affiliate;
                if (aff == null || user.JoinMethod == (int)UserJoinMethod.auto)
                    return;
                var url = aff.Url;
                var parms = user.AffiliateParams;
                if (!string.IsNullOrEmpty(parms))
                {
                    Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(parms);
                    var p = aff.param1;
                    if (!string.IsNullOrEmpty(p))
                    {

                        if (values.ContainsKey(p))
                            url = url.Replace("[param1]", values[p]);
                    }

                    p = aff.param2;
                    if (!string.IsNullOrEmpty(p))
                    {
                        if (values.ContainsKey(p))
                            url = url.Replace("[param2]", values[p]);
                    }

                    p = aff.param3;
                    if (!string.IsNullOrEmpty(p))
                    {
                        if (values.ContainsKey(p))
                            url = url.Replace("[param3]", values[p]);
                    }

                    p = aff.param4;
                    if (!string.IsNullOrEmpty(p))
                    {
                        if (values.ContainsKey(p))
                            url = url.Replace("[param4]", values[p]);
                    }

                    p = aff.param5;
                    if (!string.IsNullOrEmpty(p))
                    {
                        if (values.ContainsKey(p))
                            url = url.Replace("[param5]", values[p]);
                    }


                }
                logger.Debug("affilate url fired:" + url);
                var module = user.Affiliate.PixelFireModule;
                var count = user.Affiliate.PixelFireCount;
                AffiliateTransaction at = new AffiliateTransaction();
                at.AffiliateId = aff.Id;
                at.Requset = user.URL;//url;//change to user.URL
                at.TransactionDate = DateTime.Now;
                at.UserId = user.Id;
                at.IsLatePixel = isLate;
                at.PayOut = aff.param5;
                at.Reason = action;
                at.PixelFiredAfterAmount = aff.PixelFireAfterAmount;
                if (module == null || (module != null && count % module != 0))
                {
                    try
                    {
                        // var res = ((HttpWebRequest)WebRequest.Create(url)).GetResponse();
                        // at.ResultStatus = (((HttpWebResponse)res).StatusDescription);
                        at.ResultStatus = "";
                    }
                    catch (Exception ex)
                    {
                        at.ResultStatus = "Exception in send";
                        user.AffiliateTransactions.Add(at);
                        user.Affiliate.PixelFireCount++;
                    }
                }
                else
                {
                    at.ResultStatus = "SCRUB";
                }

                user.AffiliateTransactions.Add(at);
                user.Affiliate.PixelFireCount++;

            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}