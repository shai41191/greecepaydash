﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gateway.Models
{
    public class DIMOCOResponse
    {
        public string Status { get; set; }
        public string Description { get; set; }
        public string MsgId { get; set; }

        public override string ToString()
        {
            return string.Format("Status:{0},Description:{1},MsgId:{2}",Status,Description,MsgId);
        }
    }
}