﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class DIMOCOLookupResult
    {
        public bool Result { get; set; }
        public string Description { get; set; }
        public bool Retry { get; set; }
        public string Primary { get; set; }
        public string Secondary { get; set; }
    }
       
}


