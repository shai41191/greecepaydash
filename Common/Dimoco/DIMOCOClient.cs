﻿using Gateway.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using Common;

namespace Common
{
     public class DIMOCOClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public const string test_number = "301234";

        public const string BASE_SEND_SMS_URL = "https://services.dimoco.at/psms/send-sms";
        public const string AppIDHigh = "AppIDHigh";
        public const string AppIDLow = "AppIDLow";
        public const string KEYWORD = "keyword";
        public const string PASSWORD = "password";
        public const string TO = "to";
        public const string ACTION = "action";
        public const string TARIFF = "tariff";
        public const string DLRMASK = "dlr-mask";
        public const string MESSAGE = "message";
        public const string OPERATOR = "operator";
        public string PIN_CODE_MSG = "PincodeMessage";
        public string WELCOME_MSG = "WelcomeMessage";

        public string UNSUBSCRIBED_SMS_MSG = "UnsubscribedMessage";
        public string FIFTEEN_DAYS_UNBILLED_USERS_REMINDER_MSG = "FifteenDaysReminderMessage";
        public string MONTHLY_REMINDER_MSG = "MonthlyReminderMessage";
        public string ALREADY_SUBSCRIBER_SMS_MSG = "AlreadySubscriberMessage";
        public  WebClient webclient;
     

        private DIMOCOClient(int serviceid, bool useSmall = false)
        {
            webclient  = new WebClient();
            webclient.Encoding = Encoding.UTF8;
            webclient.BaseAddress = BASE_SEND_SMS_URL;
            if (useSmall)
            {
                webclient.QueryString.Add("app-id", ServicesProvider.GetString(serviceid, AppIDLow));
            }
            else
            {
                webclient.QueryString.Add("app-id", ServicesProvider.GetString(serviceid, AppIDHigh));
            }

            webclient.QueryString.Add("keyword", ServicesProvider.GetString(serviceid, KEYWORD));
            webclient.QueryString.Add("password", ServicesProvider.GetString(serviceid, PASSWORD));
        }



        public DIMOCOResponse SendPinCodeSMS(string to, string pinCode, string operatorCode, int serviceid)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0",MsgId = "123" };
            
            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(String.Format(ServicesProvider.GetString(serviceid, PIN_CODE_MSG), pinCode)));
            webclient.QueryString.Add(ACTION, "session_new");
            webclient.QueryString.Add(TARIFF, "free");
            webclient.QueryString.Add(DLRMASK, "27");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendPinCode");
            return ParseResponse(webclient.DownloadString(""));
        }

        public DIMOCOResponse SendWelcomeSMSForOneClick(string to, string operatorCode, int serviceid)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };

            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(ServicesProvider.GetString(serviceid, WELCOME_MSG)));
            webclient.QueryString.Add(ACTION, "session_new");
            webclient.QueryString.Add(TARIFF, "free");
            webclient.QueryString.Add(DLRMASK, "27");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendWelcomeSMS");
            return ParseResponse(webclient.DownloadString(""));
        }

        public DIMOCOResponse SendWelcomeSMS(string to, string operatorCode, int serviceid)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };

            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(ServicesProvider.GetString(serviceid, WELCOME_MSG)));
            webclient.QueryString.Add(TARIFF, "free");
            webclient.QueryString.Add(DLRMASK, "27");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendWelcomeSMS");
            return ParseResponse(webclient.DownloadString(""));
        }

        private static string AddCountryPrefix(string to)
        {
            to = "30" + to;
            return to;
        }

        public DIMOCOResponse SendUnsubscriptionSMS(string to, string operatorCode, int serviceid)
        {
            DIMOCOResponse res = null;
            try
            {
 
                to = AddCountryPrefix(to);
                webclient.QueryString.Add(TO, to);
                webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(ServicesProvider.GetString(serviceid, UNSUBSCRIBED_SMS_MSG)));
                webclient.QueryString.Add(TARIFF, "free");
                webclient.QueryString.Add(DLRMASK, "27");
                webclient.QueryString.Add(OPERATOR, operatorCode);
                LogQueryString("SendUnsubscriptionSMS");
                res = ParseResponse(webclient.DownloadString(""));
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
            return res;

        }

        public DIMOCOResponse SendAlerdaySubscriberSMS(string to, string operatorCode, int serviceid)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };

            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(ServicesProvider.GetString(serviceid, ALREADY_SUBSCRIBER_SMS_MSG)));
            webclient.QueryString.Add(TARIFF, "free");
            webclient.QueryString.Add(DLRMASK, "0");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendAlerdaySubscriberSMS");
            return ParseResponse(webclient.DownloadString(""));
        }

        public DIMOCOResponse Send15DaysUnbilledUsers(string to, string operatorCode, int serviceid)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };

            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(ServicesProvider.GetString(serviceid, FIFTEEN_DAYS_UNBILLED_USERS_REMINDER_MSG)));
            webclient.QueryString.Add(TARIFF, "free");
            webclient.QueryString.Add(DLRMASK, "27");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("Send15DaysUnbilledUsers");
            return ParseResponse(webclient.DownloadString(""));
        }

        public DIMOCOResponse SendMonthlyReminder(string to, string operatorCode, int serviceid)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };
            
            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(ServicesProvider.GetString(serviceid, MONTHLY_REMINDER_MSG)));
            webclient.QueryString.Add(TARIFF, "free");
            webclient.QueryString.Add(DLRMASK, "0");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendMonthlyReminder");
            return ParseResponse(webclient.DownloadString(""));
        }

        public DIMOCOResponse SendPaidSMSSmall(string to, string operatorCode, string msg)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };

            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(msg));
            webclient.QueryString.Add(TARIFF, "052");
            webclient.QueryString.Add(DLRMASK, "27");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendPaidSMSSmall");
            return ParseResponse(webclient.DownloadString(""));
        }

        public DIMOCOResponse SendPaidSMS(string to, string operatorCode, string msg)
        {
            if (to == test_number)
                return new DIMOCOResponse() { Status = "0", MsgId = "123" };
            
            to = AddCountryPrefix(to);
            webclient.QueryString.Add(TO, to);
            webclient.QueryString.Add(MESSAGE, HttpUtility.UrlEncode(msg));
            webclient.QueryString.Add(TARIFF, "208");
            webclient.QueryString.Add(DLRMASK, "27");
            webclient.QueryString.Add(OPERATOR, operatorCode);
            LogQueryString("SendPaidSMS");
            return ParseResponse(webclient.DownloadString(""));
        }

        public static DIMOCOLookupResult LookupMsisdn(int serviceid ,string to)
        {
            if (to == test_number)
                return new DIMOCOLookupResult() {Result=true,Primary="" };

            to = AddCountryPrefix(to);
            var webclient = new WebClient();
            webclient.Encoding = Encoding.UTF8;
            webclient.BaseAddress = "https://services.dimoco.at/mnol/v1";
            webclient.QueryString.Add("msisdn", to);
            webclient.QueryString.Add("app-id", "106293");
            webclient.QueryString.Add("digest", SHAUtil.sha256_hash("Mh1Rsx" + to));
            //LogQueryString("LookupMsisdn");
            return ParseLookupResult(webclient.DownloadString(""));
        }

        private static DIMOCOLookupResult ParseLookupResult(string str)
        {
            logger.Debug("lookup result:{0}", str);
            DIMOCOLookupResult result = new DIMOCOLookupResult();
            XElement x = XElement.Parse(str);
            if (x.Elements().First().Name == "operator") 
            {
                var op = x.Elements().First();
                result.Primary = op.Attribute("primary").Value;
                result.Secondary = op.Attribute("secondary").Value;
                result.Result = true;
            }
            else
            {
                var op = x.Elements().First();
                result.Description = op.Attribute("description").Value;
                result.Retry = bool.Parse(op.Attribute("retry").Value);
                result.Result = false;
            }
            return result;
        }

        private void LogQueryString(string action)
        {
            string str = action;
            foreach (String key in webclient.QueryString)
            {
                if (key == "password")
                    continue;

                str += ","+key +"="+ webclient.QueryString[key];
            }

            logger.Info(str);
        }

        private static DIMOCOResponse ParseResponse(string str)
        {
            logger.Debug("DIMOCOResponse result:{0}", str);
            DIMOCOResponse response = new DIMOCOResponse();
            XElement x = XElement.Parse(str);
            response.Status = x.Element("status").Value;
            response.Description = x.Element("description").Value;
            response.MsgId = x.Element("msg-id") != null ? x.Element("msg-id").Value : "";
            return response;
        }

    }
}