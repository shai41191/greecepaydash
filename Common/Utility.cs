﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

namespace Common
{

    public partial class cellEntities
    {
        partial void OnContextCreated()
        {
            this.SavingChanges += new EventHandler(context_SavingChanges);
            this.CommandTimeout = 360;
        }

        private static void context_SavingChanges(object sender, EventArgs e)
        {
            foreach (ObjectStateEntry entry in
           ((ObjectContext)sender).ObjectStateManager.GetObjectStateEntries(EntityState.Modified))
            {
                if (!entry.IsRelationship && (entry.Entity.GetType() == typeof(User)))
                {
                    User CurrentEntity = (User)entry.Entity;
                    using (cellEntities Entities = new cellEntities())
                    {
                        //if the user status is unsubscribed
                        if (CurrentEntity.Status == (int)UserStatus.unsubscribed)
                        {
                            User LookUp = Entities.Users.FirstOrDefault(a => a.Id == CurrentEntity.Id);
                            //if db user is not unsubscribed and after the save will be
                            if (LookUp.Status != CurrentEntity.Status)
                            {
                                try
                                {
                                    new ServiceProviderClient().DeleteUser(CurrentEntity.ServiceId, CurrentEntity.PhoneNumber);
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                }
            }
        }
    }


    public class ErrorCodes
    {
        public const string wrong_pincode = "1000";
        public const string cant_send_pincode = "1001";
        public const string user_already_active = "1002";
        public const string user_already_subscribed = "1003";
        public const string msisdn_lookup = "1004";
        public const string server_error = "1005";
        public const string user_pending = "1006";
    }

    public enum UserStatus
    {
        entered = 1,
        subscribed = 2,
        active =3,
        unsubscribed = 4,
        semibilled = 5,
        blacklist = 6
    }

    public enum UserJoinMethod
    {
        website = 1,
        cellular = 2,
        auto =3,
        migrated = 4
    }

    public enum UserDeleteMethod
    {
        manual = 1,
        sms = 2,
        auto = 3,
        ivr = 4
    }

    public enum UserTransactionAction
    {
        send_new_session_sms = 1,
        send_welcome_sms = 2,
        send_charged_sms = 3,
        send_3_first_charged_sms = 4,
        send_fifteen_days_remindeier_sms = 5,
        send_monthly_reminder_sms = 6,
        send_termination_of_subscription = 7,
        send_alerday_subscribed = 8

    }

    public enum UserTransactionState
    {
        sent = 1,
        failed = 2,
    }



    
    [MetadataType(typeof(User.UserMetaData))]
    public partial class User
    {
        public class UserMetaData
        {
            [EnumDataType(typeof(UserStatus))]
            public object Status { get; set; }

            [Display(Order=4)]
            [FilterUIHint("StartsWith")]
            public object PhoneNumber { get; set; }

            [DisplayName("Delete")]
            public object DeleteMethod { get; set; }

            [DisplayName("Join")]
            public object JoinMethod { get; set; }

            [ScaffoldColumn(false)]
            public object AffiliateParams { get; set; }

            [DisplayName("Aff")]
            public object Affiliate { get; set; }

            
            [DisplayName("URL")]
            public object URL { get; set; }

            [DisplayName("Count")]
            public object SentCount { get; set; }

            [DisplayName("Pin")]
            public object PinCode { get; set; }

            [FilterUIHint("DateRange")]
            [Display(Name = "Creation Date", AutoGenerateField = true, AutoGenerateFilter = true)]
            public object CreationDate { get; set; }
        }
    }

 

    [MetadataType(typeof(UserTransaction.UserMetaData))]
    public partial class UserTransaction
    {
        public class UserMetaData
        {
            [FilterUIHint("StartsWith")]
            public object State { get; set; }

            [FilterUIHint("StartsWith")]
            public object TransactionAction { get; set; }

            [FilterUIHint("GreaterThanOrEqualTo")]
            public object Tariff { get; set; }


            [FilterUIHint("StartsWith")]
            public object ErrorCode { get; set; }
            
            [ScaffoldColumn(false)]
            public object Signifier { get; set; }

            [ScaffoldColumn(false)]
            public object Udh { get; set; }

            [ScaffoldColumn(false)]
            public object Keyword { get; set; }

            [FilterUIHint("DateRange")]
            [Display(Name = "Transaction Date", AutoGenerateField = true, AutoGenerateFilter = true)]
            public object TransactionDate { get; set; }
            //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
            //public object TransactionDate { get; set; }

            //[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
            //public object DLRDate { get; set; }

            [Display(Name = "User", AutoGenerateField = true, AutoGenerateFilter = false)]
            public object User { get; set; }
        }
    }


    [MetadataType(typeof(AffiliateTransaction.AffMetaData))]
    public partial class AffiliateTransaction
    {
        public class AffMetaData
        {
            

            [FilterUIHint("DateRange")]
            [Display(Name = "Transaction Date", AutoGenerateField = true, AutoGenerateFilter = true)]
            public object TransactionDate { get; set; }

            [ScaffoldColumn(false)]
            public object Uuid { get; set; }

            [Display(Name = "User", AutoGenerateField = true, AutoGenerateFilter = false)]
            public object User { get; set; }
        }
    }


    [MetadataType(typeof(Affiliate.AffiliateMetaData))]
    public partial class Affiliate
    {
        public class AffiliateMetaData
        {
            [DisplayName("Max")]
            public object Max_Close_After_30_Days { get; set; }


            [Display(Order = 1)]
            public object Description { get; set; }
            [Display(Order = 2)]
            public object Code { get; set; }
            [Display(Order = 3)]
            public object Enabled { get; set; }
            [Display(Order = 3)]
            public object LatePixelFire { get; set; }
            [Display(Order = 4)]
            public object PixelFireAfterAmount { get; set; }
            [Display(Order = 5)]
            public object PixelFireModule { get; set; }
            
        }
    }


    public class Utility
    {
         public const string DELIVRD = "DELIVRD";

         public static string RemovePhonePrefix(string phoneNumber)
         {
             if (phoneNumber.StartsWith("30"))
                 phoneNumber = phoneNumber.Remove(0, 2); ;
             return phoneNumber;
         }

        public static int GetNetworkId(string OpreatorName)
        {
            switch (OpreatorName)
            {
                case "GR_COSMOTE":
                    return 265;
                    break;
                case "GR_WIND":
                    return 635;
                    break;
                case "GR_VODAFONE":
                    return 268;
                    break;
                default:
                    return 0;
                    break;
            }
        }

        public static string parseGenericResponse(string response)
        {
            string responseToken = "";
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Ignore;

            using (XmlReader reader = XmlReader.Create(new StringReader(response), settings))
            {
                while (reader.Read())
                {
                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        // Get element name and switch on it.
                        switch (reader.Name)
                        {
                            case "REQUESTID":
                                reader.Read();
                                responseToken = reader.Value;
                                return responseToken.Split('_').Last();
                                break;
                        }
                    }
                }
            }

            return "error in xml generic parsing";

        }
    }
}