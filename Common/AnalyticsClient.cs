﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace Common
{
    public static class AnalyticsClient
    {
        const string ENTER_URL = "https://www.google-analytics.com/collect?v=1&t=event&tid=UA-42558378-1&cid={CLIENT_ID}&ec=Entered&ea={PRODUCT_NAME}&uip={USER_IP}";
        const string SUBSCRIBED_URL = "https://www.google-analytics.com/collect?v=1&t=event&tid=UA-42558378-1&cid={CLIENT_ID}&ec=Subscribed&ea={PRODUCT_NAME}&uip={USER_IP}";
        const string FIRST_TBS_URL = "https://www.google-analytics.com/collect?v=1&t=event&tid=UA-42558378-1&cid={CLIENT_ID}&ec=First%20FBS&ea={PRODUCT_NAME}&uip={USER_IP}";
        const string LATE_TBS_URL = "https://www.google-analytics.com/collect?v=1&t=event&tid=UA-42558378-1&cid={CLIENT_ID}&ec=Late%20FBS&ea={PRODUCT_NAME}&uip={USER_IP}";
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static WebClient webclient;

        static AnalyticsClient()
        {
            webclient = new WebClient();
            webclient.Encoding = Encoding.UTF8;
        }

        static public void FireEnter(User user)
        {
            try
            {
                string url = ENTER_URL;
                Fire(user, url);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }


        static public void FireSubscribed(User user)
        {
            try
            {
                string url = SUBSCRIBED_URL;
                Fire(user, url);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        static public void FireFirstTBS(User user)
        {
            try
            {
                string url = FIRST_TBS_URL;
                Fire(user, url);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        static public void FireLateTBS(User user)
        {
            try
            {
                string url = LATE_TBS_URL;
                Fire(user, url);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }


        private static void Fire(User user, string url)
        {
            //string cid = GetCid(user);
            //if (cid == null) return;

            //url = url.Replace("{CLIENT_ID}", cid);
            //url = url.Replace("{USER_IP}", user.IP);        
            //string prod = GetProductFromURL(user.URL);
            //url = url.Replace("{PRODUCT_NAME}", prod);
            //webclient.DownloadString(url);
        }

        private static string GetCid(User user)
        {
            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(user.AffiliateParams);
            if(values.ContainsKey("cid"))
            {
                return values["cid"];
            }
            else
            {
                return null;
            }
   
        }

        private static string GetProductFromURL(string url)
        {
            string[] directories = url.Split('/');
            for (int i = 0; i < directories.Length;i++ )
            {
                if (directories.GetValue(i).Equals("gr"))
                    return directories[i + 1];
            }

            throw new Exception("Product name was not found in path");
        }
        


    }
}
