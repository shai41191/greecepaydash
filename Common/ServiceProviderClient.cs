﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ServiceProviderClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public WebClient webclient;

        public ServiceProviderClient()
        {
            webclient = new WebClient();
            webclient.Encoding = Encoding.UTF8;
        }

        public void DeleteUser(int serviceid , string phoneNumber)
        {
            try
            {
                webclient.DownloadStringAsync(new Uri(ServicesProvider.GetString(serviceid,"DisableUserEndpoint")+phoneNumber));
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void CreateUser(int serviceid, string phoneNumber,string code)
        {
            try
            {
                var url = string.Format(ServicesProvider.GetString(serviceid, "CreateUserEndpoint"), phoneNumber, code);
                webclient.DownloadString(url);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}
