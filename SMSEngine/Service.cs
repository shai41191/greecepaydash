﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using Common;
using System.Configuration;
using Common.PayDash;
using System.Xml;
using System.IO;
using System.Web;

namespace SMSEngine
{
    public partial class Service : ServiceBase
    {
        private static Logger workerlogger = LogManager.GetLogger("Worker");
        private static Logger dlrLogger = LogManager.GetLogger("DLRWorker");
        private static Logger moLogger = LogManager.GetLogger("MOWorker");
        private static Logger affLogger = LogManager.GetLogger("AFFWorker");

        const string DELIVRD = "DELIVRD";
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        private Thread _thread;
        private Thread _affThread;
        private Thread _dlrThread;
        private Thread _moThread;
        const decimal BIG_TARIFF = 2.08m;
        const decimal DAILY_SUM = BIG_TARIFF * 6;
        const decimal MONTH_SUM = BIG_TARIFF * 12;
        const decimal SMALL_SUM = MONTH_SUM - BIG_TARIFF;
        const decimal SmallTariff = 0.52m;
        const decimal WEEKLY_SUM = BIG_TARIFF * 3;
        const decimal MONTHLY_SUM = WEEKLY_SUM * 4;
        const decimal FIRST_DAY_SUM = BIG_TARIFF * 6;
        const decimal EVERY_DAY_SUM = BIG_TARIFF * 6;

        private const string OUT_OF_CREDIT = "OUT_OF_CREDIT";
        private const string MONTHLY_BILL_CAP = "MONTHLY BILL CAP";
        string send_charged_sms = UserTransactionAction.send_charged_sms.ToString();
        string send_3_first_charged_sms = UserTransactionAction.send_3_first_charged_sms.ToString();
        Dictionary<String, uspUserGetForReRegister_Result> reRegList = new Dictionary<String, uspUserGetForReRegister_Result>();

        public Service()
        {
            InitializeComponent();

        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            workerlogger.Debug("Unhandled exception in code. See the exceptions logs.");
            workerlogger.Error(e.ExceptionObject as Exception);
        }

        protected override void OnStart(string[] args)
        {

            _thread = new Thread(WorkerThreadFunc);
            _thread.Name = "My Worker Thread";
            _thread.IsBackground = true;
            _thread.Start();

            _affThread = new Thread(AffWorkerThreadFunc);
            _affThread.Name = "My Worker Thread2";
            _affThread.IsBackground = true;
            _affThread.Start();

            _dlrThread = new Thread(DLRWorkerThreadFunc);
            _dlrThread.Name = "DLR Worker";
            _dlrThread.IsBackground = true;
            _dlrThread.Start();

            _moThread = new Thread(MOWorkerThreadFunc);
            _moThread.Name = "MO Worker";
            _moThread.IsBackground = true;
            _moThread.Start();
        }

        #region MO

        public void MOWorkerThreadFunc()
        {
            moLogger.Debug("start MOWorkerThreadFunc");
            RedisClient c = new RedisClient();
            while (!_shutdownEvent.WaitOne(0))
            {

                try
                {
                    var s = c.GetNextMOXml();
                    if (s != null)
                        ThreadPool.QueueUserWorkItem(ProcessMO, s);
                    else
                        Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    moLogger.Fatal(ex);
                }
            }
        }

        public void ProcessMO(object XML)
        {
            try
            {
                moLogger.Debug($"ProcessMO: {XML}");
                var res = LoadMOXml(XML.ToString());

                string text = res.TEXT;

                string from = Utility.RemovePhonePrefix(res.SOURCE_ADDR);

                using (var db = new cellEntities())
                {
                    var users = new List<User>();
                    if (text.ToUpper().Contains("STOP"))
                    {
                        users = db.Users.Where(a => a.PhoneNumber == from).ToList();
                        foreach (var user in users)
                        {
                            if (user.Status == (int)UserStatus.blacklist || user.Status == (int)UserStatus.unsubscribed || user.Status == (int)UserStatus.entered)
                                continue;

                            var tran = new UserTransaction();
                            tran.UserId = user.Id;
                            tran.ServiceId = user.ServiceId;
                            tran.PhoneNumber = from;
                            tran.TransactionDate = DateTime.Now;
                            tran.Message = text;
                            tran.Operator = res.NETWORKID;
                            tran.Udh = "";
                            //tran.Signifier = signifier;
                            tran.TransactionAction = "MO";
                            db.UserTransactions.AddObject(tran);
                            db.SaveChanges();
                            //if (KEYWORD  STOP) remove user , we should decativate him

                            moLogger.Debug("MO cell stop user:{0} with phone number {1}", user.Id, from);
                            user.Status = (int)UserStatus.unsubscribed;
                            user.DeleteMethod = (int)UserDeleteMethod.sms;
                            //block user
                            //new PlayandwinClient().DeleteUser(user.PhoneNumber);
                            //send UNSUBSCRIPTION sms
                            SendunsbscriptionSMS(db, user);
                            //no need to save to db its in the write transaction
                            moLogger.Info("user {0} was unsubscribed", user.Id);

                        }
                    }

                    if (text.ToUpper().Contains("OK"))
                    {
                        users = db.Users.Where(a => a.PhoneNumber == from && a.Status == (int)UserStatus.entered).ToList();
                        var user = users.LastOrDefault();
                        PayDashClient client = new PayDashClient(user.ServiceId);
                        string seqid = new DAL().GetNextTransactionId();
                        new ServiceProviderClient().CreateUser(user.ServiceId, user.PhoneNumber, user.PinCode);
                        var response = client.SendWelcomeSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);
                        user.PinCode = "1111";
                        user.Status = (int)UserStatus.subscribed;
                        WriteTransaction(db, user, seqid, response, UserTransactionAction.send_welcome_sms.ToString(), ServicesProvider.GetString(user.ServiceId, "WelcomeMessage"));
                        db.SaveChanges();
                        moLogger.Info("send Welcome SMS response: " + response);
                    }

                    if (users.Count() == 0)
                    {
                        moLogger.Error("Could not find the user by phone number:{0} for MO", from);
                        return;
                    }



                }
            }
            catch(Exception ex)
            {
                moLogger.Fatal(ex);
            }
        }

        private TPBOUND_MESSAGES LoadMOXml(string xml)
        {
            TPBOUND_MESSAGES re = new TPBOUND_MESSAGES();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Ignore;

            using (XmlReader reader = XmlReader.Create(new StringReader(xml), settings))
            {
                while (reader.Read())
                {
                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        // Get element name and switch on it.
                        switch (reader.Name)
                        {
                            case "SERVICEID":
                                reader.Read();
                                re.SERVICEID = int.Parse(reader.Value);
                                break;
                            case "TEXT":
                                reader.Read();
                                re.TEXT = reader.Value;
                                break;
                            case "SOURCE_ADDR":
                                reader.Read();
                                re.SOURCE_ADDR = reader.Value;
                                break;
                            case "GWTRANSACTIONID":
                                reader.Read();
                                re.GWTRANSACTIONID = reader.Value;
                                break;
                            case "DESTINATION_ADDR":
                                reader.Read();
                                re.DESTINATION_ADDR = reader.Value;
                                break;
                            case "NETWORKID":
                                reader.Read();
                                re.NETWORKID = reader.Value;
                                break;
                        }
                    }
                }
            }

            return re;
        }

        


        #endregion


        #region DLR
        public void DLRWorkerThreadFunc()
        {
            dlrLogger.Debug("start DLRWorkerThreadFunc");
            RedisClient c = new RedisClient();
            while (!_shutdownEvent.WaitOne(0))
            {

                try
                {
                    var s = c.GetNextDLRXml();
                    if (s != null)
                        ThreadPool.QueueUserWorkItem(ProcessDLR, s);
                    else
                        Thread.Sleep(100);
                }
                catch (Exception ex)
                {
                    dlrLogger.Fatal(ex);
                }
            }
        }

        public void ProcessDLR(object dlr)
        {
            try
            {
                dlrLogger.Debug("processDLR");
                RECEIPTS res = LoadDLRXml(dlr.ToString());
                string msgid = res.SMSRECEIPT.TRANSACTIONID;
                string state = res.SMSRECEIPT.STATUSID.ToString();
                dlrLogger.Info("req xml: " + dlr.ToString());


                using (var db = new cellEntities())
                {
                    var tran = db.UserTransactions.Where(a => a.MsgId == msgid).FirstOrDefault();
                    if (tran == null)
                    {
                        dlrLogger.Error("retry find the message id:{0} for DLR", msgid);
                        Thread.Sleep(200);
                        //lets do another try 
                        tran = db.UserTransactions.Where(a => a.MsgId == msgid).FirstOrDefault();
                        if (tran == null)
                        {
                            dlrLogger.Error("Could not find the message id:{0} for DLR", msgid);
                            return;
                        }
                    }
                    if (tran.DLRDate != null)
                    {
                        dlrLogger.Error("message id:{0} alredy got DLR", msgid);
                        return;
                    }

                    tran.State = GetStateString(state);
                    tran.DLRDate = DateTime.Now;
                    ////tran.Retry = retry;
                    ////tran.ErrorDescription = error;
                    ////tran.ErrorCode = reasoncode;
                    int count = 1;

                    if (tran.State == "13" && tran.TransactionAction != UserTransactionAction.send_fifteen_days_remindeier_sms.ToString() && tran.User.Status != (int)UserStatus.unsubscribed && tran.User.Status != (int)UserStatus.blacklist)
                    {
                        db.uspUserStatusUpdate(tran.User.Id, (int)UserStatus.blacklist, (int)UserDeleteMethod.auto);
                        new ServiceProviderClient().DeleteUser(tran.User.ServiceId, tran.User.PhoneNumber);
                        SendunsbscriptionSMS(db, tran.User);
                    }

                    //we have failed to send big Tariff lets try small
                    if ((tran.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || tran.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && (tran.State == OUT_OF_CREDIT || tran.State == MONTHLY_BILL_CAP) && tran.Tariff == BIG_TARIFF)
                    {
                        SendPaidSmall(db, tran);
                    }


                    //if this is DLR for welcome send the first 6 paid sms
                    if (tran.TransactionAction == UserTransactionAction.send_welcome_sms.ToString() && tran.State == Utility.DELIVRD)
                    {
                        dlrLogger.Info("send first after welcome");
                        //sending the first
                        SendFirstPaid(1, db, tran);
                    }

                    if (tran.TransactionAction == UserTransactionAction.send_fifteen_days_remindeier_sms.ToString() && tran.State != Utility.DELIVRD)
                    {
                        tran.User.Status = (int)UserStatus.unsubscribed;
                        tran.User.DeleteMethod = (int)UserDeleteMethod.auto;
                        SendunsbscriptionSMS(db, tran.User);
                    }

                    //if cahrged small First Day contuine with small 
                    if (tran.Tariff == SmallTariff && tran.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString() && tran.State == Utility.DELIVRD)
                    {
                        var sum = UpdateChargedUser(db, tran);

                        int.TryParse(tran.Signifier, out count);
                        if (sum < FIRST_DAY_SUM)
                        {
                            SendFirstPaidSmall(db, tran);
                        }
                    }

                    //if cahrged small every day contuine with small 
                    if (tran.Tariff == SmallTariff && tran.TransactionAction == UserTransactionAction.send_charged_sms.ToString() && tran.State == Utility.DELIVRD)
                    {
                        var sum = UpdateChargedUser(db, tran);


                        if (sum < MONTHLY_SUM)
                        {

                            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                            var trans = tran.User.UserTransactions.Where(a => (a.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || a.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && a.State == Utility.DELIVRD && a.TransactionDate > dt);
                            var todaySum = trans.Sum(a => a.Tariff);//today sum

                            int.TryParse(tran.Signifier, out count);
                            if (todaySum < EVERY_DAY_SUM)
                            {
                                SendPaidSmall(db, tran);
                            }
                        }
                    }


                    //First day of the month or first day of reg
                    if (tran.Tariff == BIG_TARIFF && tran.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString() && tran.State == Utility.DELIVRD)
                    {
                        var sum = UpdateChargedUser(db, tran);
                        //get transaction sms count
                        int.TryParse(tran.Signifier, out count);
                        count = count % 3;
                        count++;

                        if (sum < FIRST_DAY_SUM)
                        {
                            SendFirstPaid(count, db, tran);
                        }

                    }

                    //every day 
                    if (tran.Tariff == BIG_TARIFF && tran.TransactionAction == UserTransactionAction.send_charged_sms.ToString() && tran.State == Utility.DELIVRD)
                    {
                        UpdateChargedUser(db, tran);
                        var startOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        var monthlyTrans = tran.User.UserTransactions.Where(a => (a.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || a.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && a.State == Utility.DELIVRD && a.TransactionDate > startOfMonth);
                        //get sum of trans
                        var monthlySum = monthlyTrans.Count() == 0 ? 0 : monthlyTrans.Sum(a => a.Tariff);
                        if (monthlySum < MONTHLY_SUM)
                        {
                            DateTime dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                            var trans = tran.User.UserTransactions.Where(a => (a.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || a.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && a.State == Utility.DELIVRD && a.TransactionDate > dt);
                            var todaySum = trans.Sum(a => a.Tariff);//today sum
                                                                    //get transaction sms count
                            int.TryParse(tran.Signifier, out count);
                            count = count % 3;
                            count++;

                            if (todaySum < EVERY_DAY_SUM)
                            {
                                SendPaid(count, db, tran.User, tran.ProcessId);
                            }
                        }

                    }

                    //save transaction
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                dlrLogger.Fatal(ex);
            }

        }

        private decimal? UpdateChargedUser(cellEntities db, UserTransaction tran)
        {
            //update user balance
            db.uspUserBalanceUpdate(tran.Tariff.GetValueOrDefault(), tran.User.Id);

            if (tran.User.Status == (int)UserStatus.subscribed || tran.User.Status == (int)UserStatus.semibilled || tran.User.Status == (int)UserStatus.active)
            {
                var dt = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                //get transaction count if we are in state of first paid or in semi
                var trans = tran.User.UserTransactions.Where(a => (a.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || a.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && a.State == Utility.DELIVRD && a.TransactionDate > dt);
                var sum = trans.Sum(a => a.Tariff);

                if (sum == MONTHLY_SUM)
                {
                    tran.User.Status = (int)UserStatus.active;
                }
                if (sum < MONTHLY_SUM)
                {
                    tran.User.Status = (int)UserStatus.semibilled;
                }

                if (tran.User.Affiliate != null)
                {
                    var pixalLimit = tran.User.Affiliate.PixelFireAfterAmount != null ? tran.User.Affiliate.PixelFireAfterAmount.Value : WEEKLY_SUM;
                    dlrLogger.Debug($"user: {tran.User.Id}, pixalLimit: { pixalLimit}, sum: {sum}");
                    if (sum == pixalLimit)
                    {
                        //only users that have been created in the last 24 hours can get pixel
                        if (tran.User.CreationDate.AddHours(24) >= DateTime.Now)
                        {
                            //if we are active and this is first 3 in 24 hours 
                            if (tran.User.Affiliate != null && tran.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString())
                            {
                                HandleFirstOnlyPixal(tran);
                            }
                        }

                    }

                        //is this late pixel user and we are in the 30 days from regestraion
                        if (tran.User.Affiliate.LatePixelFire && DateTime.Today != tran.User.CreationDate.Date && ((DateTime.Now - tran.User.CreationDate).Days < 30))
                        {
                            HandleLatePixel(tran);
                        }
                }

                return sum;
            }
            return int.MaxValue;
        }

        private void HandleLatePixel(UserTransaction tran)
        {
            //הפיקסל יצא לאחר 3 חיובים ליוזר ולא משנה אם הם רצופים או לא ואם הם בהרשמה או לא בתאני שהם בטווח של עד 30 יום מתאריך ההרשמה
            var trans = tran.User.UserTransactions.Where(a => (a.TransactionAction == UserTransactionAction.send_charged_sms.ToString() || a.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString()) && a.State == Utility.DELIVRD);
            var sum = trans.Sum(a => a.Tariff);

            // var pixalLimit = tran.User.Affiliate.PixelFireAfterAmount != null ? tran.User.Affiliate.PixelFireAfterAmount.Value : WEEKLY_SUM;
             var pixalLimit = tran.User.Affiliate.PixelFireAfterAmount != null ? tran.User.Affiliate.PixelFireAfterAmount.Value : WEEKLY_SUM;

            dlrLogger.Debug($"user: {tran.User.Id}, pixalLimit: { pixalLimit}, sum: {sum}");
            if (sum == pixalLimit)
            {
                PixelClient.SendAffilatePixel(tran.User, true, "Active");
                //var res = PixelClient.SendUrl(tran.User.URL + "&IsLate=1&country=1&phoneNumber=" + tran.User.PhoneNumber, tran.User.Id);//change by shai 13-08-2020 10:04 
            }
        }

        private void HandleFirstOnlyPixal(UserTransaction tran)
        {
            dlrLogger.Debug("in pixel flow start for user {0}", tran.User.Id);
            //var baselineDate = tran.User.CreationDate.AddHours(24);

            //var tt = tran.User.UserTransactions.Where(p => p.TransactionAction == UserTransactionAction.send_3_first_charged_sms.ToString() && p.TransactionDate <= baselineDate).OrderBy(p=>p.TransactionDate).Take(3);
            ////if this is the first 3 and all of the 3 are delivered fire the pixael.
            //if (tran.User.AffiliateID.HasValue && tt.Count() == 3 && tt.All(b => b.State == Utility.DELIVRD))
            //{
            dlrLogger.Debug("in pixel flow: this is the first 3 and all of the 3 are delivered for user {0}", tran.User.Id);
            var count30 = tran.User.Affiliate.Max_Close_After_30_Days ?? 1;
            var afft = tran.User.AffiliateTransactions.Where(p => p.UserId == tran.UserId && p.TransactionDate >= DateTime.Now.AddDays(-30));
            if (afft.Count() < count30)
            {
                PixelClient.SendAffilatePixel(tran.User, false, "Active");
                var res = PixelClient.SendUrl(tran.User.URL + "&country=1&phoneNumber=" + tran.User.PhoneNumber, tran.User.Id);//change by shai 30-04-2020
            }
            //}
        }

        private void SendFirstPaidSmall(cellEntities db, UserTransaction tran)
        {
            if (tran.User.Status == (int)UserStatus.blacklist || tran.User.Status == (int)UserStatus.unsubscribed)
                return;

            User user = tran.User;
            string seqid = new DAL().GetNextTransactionId();
            PayDashClient client = new PayDashClient(user.ServiceId);
            Random r = new Random();
            int count = r.Next(1, 4);
            var msg = string.Format(ServicesProvider.GetString(user.ServiceId, "PaidMessage" + count), user.PhoneNumber, user.PinCode);
            var response = client.SendPaidSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid, msg, true);
            WriteTransaction(db, user, client, seqid, response, UserTransactionAction.send_3_first_charged_sms.ToString(), msg, count, tran.ProcessId, false, true);
        }

        private void SendFirstPaid(int count, cellEntities db, UserTransaction tran)
        {
            if (tran.User.Status == (int)UserStatus.blacklist || tran.User.Status == (int)UserStatus.unsubscribed)
                return;

            string seqid = new DAL().GetNextTransactionId();
            var user = tran.User;
            PayDashClient client = new PayDashClient(user.ServiceId);
            var msg = ServicesProvider.GetString(user.ServiceId, "FirstPaidMessage" + count);
            msg = msg.Replace("{0}", user.PhoneNumber);
            msg = msg.Replace("{1}", user.PinCode);

            var response = client.SendPaidSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid, msg);
            WriteTransaction(db, user, client, seqid, response, UserTransactionAction.send_3_first_charged_sms.ToString(), msg, count, tran.ProcessId, isFree: false);
        }

        private void SendPaid(int count, cellEntities db, User user, int? processId)
        {
            if (user.Status == (int)UserStatus.blacklist || user.Status == (int)UserStatus.unsubscribed)
                return;

            string seqid = new DAL().GetNextTransactionId();
            PayDashClient client = new PayDashClient(user.ServiceId);
            var msg = string.Format(ServicesProvider.GetString(user.ServiceId, "PaidMessage" + count), user.PhoneNumber, user.PinCode);
            var response = client.SendPaidSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid, msg);
            WriteTransaction(db, user, client, seqid, response, UserTransactionAction.send_charged_sms.ToString(), msg, count, processId, false);
        }

        private void SendPaidSmall(cellEntities db, UserTransaction tran)
        {
            if (tran.User.Status == (int)UserStatus.blacklist || tran.User.Status == (int)UserStatus.unsubscribed)
                return;

            User user = tran.User;
            string seqid = new DAL().GetNextTransactionId();
            PayDashClient client = new PayDashClient(user.ServiceId);
            Random r = new Random();
            int count = r.Next(1, 4);
            var msg = string.Format(ServicesProvider.GetString(user.ServiceId, "PaidMessage" + count), user.PhoneNumber, user.PinCode);
            var response = client.SendPaidSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid, msg, true);
            WriteTransaction(db, user, client, seqid, response, tran.TransactionAction, msg, count, tran.ProcessId, false, true);
        }

        private void SendunsbscriptionSMS(cellEntities db, Common.User user)
        {
            string seqid = new DAL().GetNextTransactionId();
            PayDashClient c = new PayDashClient(user.ServiceId);
            var res = c.SendUnsubscriptionSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);
            WriteTransaction(db, user, c, seqid, res, UserTransactionAction.send_termination_of_subscription.ToString(), ServicesProvider.GetString(user.ServiceId, "UnsubscribedMessage"));
        }

        private string GetStateString(string state)
        {
            string res = "";
            switch (state)
            {
                case "5":
                    res = "DELIVRD";
                    break;
                case "6":
                    res = "Failed @ Operator";
                    break;
                case "7":
                    res = "Failed @ Gateway";
                    break;
                case "10":
                    res = "Formatting Error – SMS too long";
                    break;
                case "11":
                    res = "Unknown Subscriber";
                    break;
                case "12":
                    res = OUT_OF_CREDIT;
                    break;
                case "19":
                    res = OUT_OF_CREDIT;
                    break;
                case "279":
                    res = MONTHLY_BILL_CAP;
                    break;
                case "280":
                    res = OUT_OF_CREDIT;
                    break;
                default:
                    res = state;
                    break;
            }

            return res;
        }

        private RECEIPTS LoadDLRXml(string xml)
        {
            RECEIPTS re = new RECEIPTS();
            re.SMSRECEIPT = new RECEIPTSSMSRECEIPT();

            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Ignore;

            using (XmlReader reader = XmlReader.Create(new StringReader(xml), settings))
            {
                while (reader.Read())
                {
                    // Only detect start elements.
                    if (reader.IsStartElement())
                    {
                        // Get element name and switch on it.
                        switch (reader.Name)
                        {
                            case "SERVICEID":
                                reader.Read();
                                re.SMSRECEIPT.SERVICEID = int.Parse(reader.Value);
                                break;
                            case "SOURCE_ADDR":
                                reader.Read();
                                re.SMSRECEIPT.SOURCE_ADDR = reader.Value;
                                break;
                            case "TRANSACTIONID":
                                reader.Read();
                                re.SMSRECEIPT.TRANSACTIONID = reader.Value;
                                break;
                            case "STATUSID":
                                reader.Read();
                                re.SMSRECEIPT.STATUSID = int.Parse(reader.Value);
                                break;
                        }
                    }
                }
            }

            return re;
        }

        private void WriteTransaction(cellEntities db, Common.User user, PayDashClient client, string msgid, string response, string action, string msg, int count = 0, int? ProcessId = 0, bool isFree = true, bool isSmall = false)
        {
            string status = Utility.parseGenericResponse(response);
            var tariff = isSmall == true ? SmallTariff : BIG_TARIFF;
            var trans = new UserTransaction();
            trans.TransactionAction = action;
            trans.Tariff = isFree ? 0 : tariff;
            trans.PhoneNumber = user.PhoneNumber;
            trans.Operator = user.Operator;
            trans.Message = msg;
            //if (decimal.TryParse(client.webclient.QueryString[DIMOCOClient.TARIFF], out tariff))
            //{
            //    if (tariff != 0)
            //        trans.Tariff = tariff/100;
            //}
            trans.UserId = user.Id;
            trans.ProcessId = ProcessId;
            trans.AffiliateID = user.AffiliateID;
            trans.ServiceId = user.ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.MsgId = msgid;
            trans.Signifier = count.ToString();
            if (status == "R0")
            {
                trans.State = UserTransactionState.sent.ToString(); ;
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorCode = status;
            }

            db.UserTransactions.AddObject(trans);
            db.SaveChanges();
        }

        private void WriteTransaction(cellEntities db, string PhoneNumber, int UserId, int ServiceId, int? AffiliateID, PayDashClient client, string response, string action, int? count, int procId, string seqid, string msg, bool isFree = false)
        {
            string status = Utility.parseGenericResponse(response);

            var trans = new UserTransaction();
            trans.TransactionAction = action;
            //trans.Keyword = ConfigurationManager.AppSettings[DIMOCOClient.KEYWORD];
            trans.Message = msg;//HttpUtility.UrlDecode(client.webclient.QueryString[DIMOCOClient.MESSAGE]);
            trans.PhoneNumber = PhoneNumber;

            if (!isFree)
                trans.Tariff = BIG_TARIFF;
            trans.UserId = UserId;
            trans.ServiceId = ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.MsgId = seqid;
            trans.AffiliateID = AffiliateID;
            trans.ProcessId = procId;
            trans.Signifier = count.ToString(); //the engine mark the sms count
            if (status == "R0")
            {
                trans.State = UserTransactionState.sent.ToString();
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorCode = status;

            }

            db.UserTransactions.AddObject(trans);

        }

        private void WriteTransaction(cellEntities db, Common.User user, string msgid, string response, string action, string message = "")
        {
            string status = Utility.parseGenericResponse(response);
            var trans = new UserTransaction();
            trans.TransactionAction = action;
            trans.Message = message;
            trans.PhoneNumber = user.PhoneNumber;
            trans.Tariff = 0;
            trans.AffiliateID = user.AffiliateID;
            trans.UserId = user.Id;
            trans.ServiceId = user.ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.MsgId = msgid;
            trans.Operator = user.Operator;
            if (status == "R0")
            {
                trans.State = UserTransactionState.sent.ToString();
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorCode = status;
            }

            db.UserTransactions.AddObject(trans);

        }
        #endregion

        #region aff
        public void AffWorkerThreadFunc()
        {
            try
            {
                var ScheduleTimeArray = new string[] { "10:00", "13:00", "16:00", "19:00", "22:00" };
                while (!_shutdownEvent.WaitOne(0))
                {

                    try
                    {
                        var CurrentHourMin = DateTime.Now.ToString("HH:mm");
                        affLogger.Debug("in aff engine loop CurrentHourMin:{0}", CurrentHourMin);
                        if (ScheduleTimeArray.Contains(CurrentHourMin))
                        {
                            affLogger.Debug("RunAffJob");

                            using (var db = new cellEntities())
                            {
                                var affs = db.Affiliates;
                                foreach (var aff in affs)
                                {
                                    if (aff.PixelFireGoal == null || aff.PixelFireGoal == 0)
                                        continue;
                                    affLogger.Debug($"RunAffJob aff id:{aff.Id}");
                                    var allUsers = GetAllTodayUsersReg(db, aff.Id);
                                    if (allUsers.Count() == 0)
                                        continue;

                                    var pixelCount = db.uspAffAffiliateTransactionsFromDate(aff.Id, DateTime.Today).First();
                                    affLogger.Debug($"pixelCount :{pixelCount}");
                                    var goal = allUsers.Count() / aff.PixelFireGoal;
                                    affLogger.Debug($"goal :{goal}");
                                    //if did not reach the goal today
                                    if (pixelCount < goal)
                                    {
                                        var remin = goal - pixelCount;
                                        affLogger.Debug($"remin :{remin}");
                                        var users = allUsers.Where(x => x.Status == 2).Take(remin.Value).ToList();
                                        foreach (var user in users)
                                        {

                                            PixelClient.SendAffilatePixel(user, false, "Goal");
                                        }

                                    }

                                }
                                db.SaveChanges();
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        affLogger.Error(ex);
                    }
                    finally
                    {
                        System.Threading.Thread.Sleep(59000);
                    }
                }
            }
            catch (Exception ex)
            {
                affLogger.Error(ex);
            }
        } 
        #endregion

        #region daily

        public List<User> GetAllTodayUsersReg(cellEntities db, int affId)
        {

            var users = db.Users.Where(x => x.AffiliateID == affId && x.CreationDate > DateTime.Today).ToList();
            return users;

        }
        public  void Month3NotDelivrdChrge()
        {
            try
            {
                using (cellEntities db = new cellEntities())
                {
                    var month3 = DateTime.Today.AddMonths(-3).Date;

                    var usersNoDelivrd = db.Users.Where(s => (s.Status == 2) && s.CreationDate <= month3 && !s.UserTransactions.Where(t => t.TransactionAction.Contains("charged")
                    && t.State == "DELIVRD" && t.TransactionDate >= month3).Any());

                    var list = new List<User>(usersNoDelivrd);
                    foreach (var obj in list)
                    {
                        obj.Status = 4;
                    }
                    db.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }
        }

        public void WorkerThreadFunc()
        {
            try
            {
                string ScheduleTime = ConfigurationManager.AppSettings["STARTING_TIME_HOUR"];
                string ScheduleCalcStatTime = ConfigurationManager.AppSettings["CALCSTAT_TIME_HOUR"];

                string[] ScheduleTimeArray = ScheduleTime.Split(',');
                DayOfWeek ScheduleDay = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), ConfigurationManager.AppSettings["STARTING_TIME_DAY"]);
                string CurrentHourMin = null;



                while (!_shutdownEvent.WaitOne(0))
                {

                    try
                    {
                        CurrentHourMin = DateTime.Now.ToString("HH:mm");
                        workerlogger.Debug("in engine loop ScheduleTime:{0}, now:{1}", ScheduleTime, CurrentHourMin);
                        if (ScheduleTimeArray.Contains(CurrentHourMin))
                        {
                            //if this is the first day of the week and the first run of the week
                            if (DateTime.Now.Date == new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) && ScheduleTimeArray[0] == CurrentHourMin)
                            {
                                workerlogger.Debug("in engine loop ScheduleDay :{0}, today :{1}", ScheduleDay, DateTime.Now.DayOfWeek);
                                RunMonthlyJob();
                            }
                            else
                            {
                                //else run the daily job
                                RunDailyJob(ScheduleDay);
                            }
                            //if this is the first run of the day run reminders
                            if (ScheduleTimeArray[0] == CurrentHourMin)
                            {
                                RunRemindeiers();
                                Month3NotDelivrdChrge();//14/06/2021 changed by shai bahar
                            }
                        }



                        if (ScheduleCalcStatTime == CurrentHourMin)
                        {
                            CalculateDailyStat();
                        }


                    }
                    catch (Exception ex)
                    {
                        workerlogger.Error(ex);
                    }
                    finally
                    {
                        System.Threading.Thread.Sleep(59000);
                    }
                }
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }
        }

        private void RunAutoRegJob(int reRegCount)
        {
            //this is for reg 
            ZeroAutoRegCounter();
            //this auto join
            reRegList = GetReRegList(reRegCount);
        }

        public Dictionary<String, uspUserGetForReRegister_Result> GetReRegList(int count)
        {
            var ls = new Dictionary<String, uspUserGetForReRegister_Result>();
            Random random = new Random();


            Random rnd = new Random();
            using (var db = new cellEntities())
            {
                var lst = db.uspUserGetForReRegister(count);
                foreach (var l in lst)
                {
                    int minutes = random.Next(300);//12 hours
                    String t = DateTime.Now.AddMinutes(minutes).ToString("HH:mm");
                    ls.Add(t, l);
                }
                return ls;
            }
        }

        private static void ZeroAutoRegCounter()
        {
            using (var db = new cellEntities())
            {
                var services = db.Services;
                foreach (var service in services)
                {
                    service.AutoRegisterCount = 0;
                }
                db.SaveChanges();
            }
        }

        public void RunRemindeiers()
        {
            workerlogger.Debug("RunRemindeiers");
            using (var db = new cellEntities())
            {
                //send monthely reminder
                var users = db.uspUserGetAllLive();
                if (DateTime.Now.Day < 28)
                {
                    foreach (var u in users)
                    {
                        //if this is the day in the month that we enetered the system send the monthely reminder
                        if (u.CreationDate.Day == DateTime.Now.Day && u.CreationDate.Date != DateTime.Today)
                            SendSendMonthlyReminderSMS(u.Id, u.Operator, u.PhoneNumber, u.ServiceId, u.AffiliateID);
                    }
                }
                else if (DateTime.Now.Day == 28)
                {
                   var uu = users.Where(u => u.CreationDate.Day >= 28 && u.CreationDate.Date != DateTime.Today);
                    foreach(var u in uu)
                        SendSendMonthlyReminderSMS(u.Id, u.Operator, u.PhoneNumber, u.ServiceId, u.AffiliateID);
                }

                //send ffiten Days reminder
                var fusers = db.uspUserTransactionsffitenDaysGet();// מי שהיה לו דליברד אחרון בדיוק לפני 15 ימים
                foreach (var u in fusers)
                {
                    var trans = db.UserTransactions.Where(a => a.UserId == u.Id).ToList();
                    //add if the user is active and last tran is from more then 15 days 
                    if (trans.Any(r => r.TransactionDate.Value.Date == DateTime.Now.AddDays(-15).Date) && (trans.OrderBy(x => x.TransactionDate).Last().State != DELIVRD || u.Status==3))
                    {
                        workerlogger.Debug("SendffitenDaysRemindeierSMS for user: {0}", u.Id);
                        SendffitenDaysRemindeierSMS(u.Id, u.Operator, u.PhoneNumber, u.ServiceId, u.AffiliateID);
                    }
                }
            }

        }

        public void RunMonthlyJob()
        {
            workerlogger.Debug("RunMonthlyJob");


            using (var db = new cellEntities())
            {
                var p = CreateProcessInDB(db);
                var users = db.uspUserGetAllLive();

                db.uspUserUpdateAllToSubscribed();
                var startdate = DateTime.Now;
                int count = 0;
                foreach (var u in users.OrderByDescending(x => x.Balance))
                {
                    try
                    {
                        workerlogger.Debug("process user id: {0}", u.Id);
                        //if user joined today do not send weekly
                        if (u.CreationDate.Date == DateTime.Today)
                            continue;
                        workerlogger.Debug("send 3 paid to user id: {0}", u.Id);
                        //set user as subscribed in the start of week 
                        //u.Status = (int)UserStatus.subscribed;
                        SendFirstPaidOfTheMonth(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, 1, p);
                    }
                    catch (Exception ex)
                    {
                        workerlogger.Error(ex);
                    }
                    finally
                    {
                        Thread.Sleep(300);
                    }
                    count++;
                }

                UpdateProcessInDB(db, p, count);
            }
        }

        private void SendFirstPaidOfTheMonth(int UserId, string PhoneNumber, int ServiceId, int? AffiliateID, string Operator, string PinCode, int? sentMsgsCount, Common.Process proc, bool isSmall = false)
        {
            try
            {
                using (var db = new cellEntities())
                {
                    PayDashClient client = new PayDashClient(ServiceId);
                    string seqid = new DAL().GetNextTransactionId();
                    string msg = string.Format(ServicesProvider.GetString(ServiceId, "PaidMessage" + sentMsgsCount), PhoneNumber, PinCode);
                    var response = client.SendPaidSMS(PhoneNumber, Operator, ServiceId, seqid, msg, isSmall);

                    WriteTransaction(db, PhoneNumber, UserId, ServiceId, AffiliateID, client, response, UserTransactionAction.send_3_first_charged_sms.ToString(), sentMsgsCount, proc.Id, seqid, msg);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }
        }

        private static void UpdateProcessInDB(cellEntities db, Common.Process p, int usersCount)
        {
            p.EndDate = DateTime.Now;
            p.UsersCount = usersCount;
            db.SaveChanges();
        }

        private static Common.Process CreateProcessInDB(cellEntities db)
        {
            Common.Process p = new Common.Process();
            p.StartDate = DateTime.Now;
            db.Processes.AddObject(p);
            db.SaveChanges();
            return p;
        }

        public void RunDailyJob(DayOfWeek startOfWeek)
        {
            var startdate = DateTime.Now;
            try
            {

                var startOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

                using (var db = new cellEntities())
                {
                    workerlogger.Debug("RunDailyJob");
                    var p = CreateProcessInDB(db);

                    var users = db.uspUserGetAllLive();
                    int count = 0;
                    foreach (var u in users)
                    {
                        count++;
                        if (u.CreationDate.Date == DateTime.Today)
                            continue;
                        if (u.Status == (int)UserStatus.active)
                            continue;

                        try
                        {
                            workerlogger.Debug("process user id: {0}", u.Id);

                            //get trans of this month
                            var monthlyTrans = db.UserTransactions.Where(b => b.UserId == u.Id && (b.TransactionAction == send_charged_sms || b.TransactionAction == send_3_first_charged_sms) && b.TransactionDate.Value > startOfMonth);

                            //we got cap this month
                            if (monthlyTrans.Any(x => x.Tariff == 0.52m && x.State == "MONTHLY BILL CAP"))
                            {
                                continue;
                            }

                            monthlyTrans = monthlyTrans.Where(b => b.State == DELIVRD);

                            //get sum of trans
                            var monthlySum = monthlyTrans.Count() == 0 ? 0 : monthlyTrans.Sum(a => a.Tariff);
                            if (monthlySum >= MONTH_SUM)
                            {
                                continue;
                            }

                            //if we still didnt get the first day cap of 6 send the first day msg
                            if (monthlySum < BIG_TARIFF * 6)
                            {
                                SendFirstPaidOfTheMonth(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, 1, p);
                            }
                            else
                            {
                                var dailyTrans = db.UserTransactions.Where(b => b.UserId == u.Id && (b.TransactionAction == send_charged_sms || b.TransactionAction == send_3_first_charged_sms) && b.State == DELIVRD && b.TransactionDate.Value > today);
                                var dailySum = dailyTrans.Count() == 0 ? 0 : dailyTrans.Sum(a => a.Tariff);
                                //what is the big tariff trans count
                                var bigTransCount = monthlyTrans.Count(a => a.Tariff == BIG_TARIFF);
                                bigTransCount = new Random().Next(1, 3);
                                if (dailySum > SMALL_SUM && dailySum < DAILY_SUM)//if we are more the 8* 0.52 we cant send 2.08 we should send 0.52
                                {
                                    //send small  small 0.52 sms do not add to count +1 
                                    workerlogger.Debug("send small paid sms to user id: {0} with sum:{1}", u.Id, dailySum);
                                    SendPaidbatch(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, bigTransCount, p, true);
                                }//send big
                                else if (dailySum < DAILY_SUM)
                                {
                                    workerlogger.Debug("send {0} paid sms to user id: {1} with sum:{2}", 3 - monthlyTrans.Count(), u.Id, monthlySum);
                                    SendPaidbatch(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, bigTransCount, p);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            workerlogger.Error(ex);
                        }
                    }
                    UpdateProcessInDB(db, p, count);
                }
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }
        }

        public void RunUserDaily(int uid)
        {
            var startOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);

            using (var db = new cellEntities())
            {
                var p = CreateProcessInDB(db);
                var u = db.Users.Where(X => X.Id == uid).Single();
                if (u.CreationDate.Date == DateTime.Today)
                    return;
                if (u.Status == (int)UserStatus.active)
                    return;

                try
                {
                    workerlogger.Debug("process user id: {0}", u.Id);

                    //get trans of this month
                    var monthlyTrans = db.UserTransactions.Where(b => b.UserId == u.Id && (b.TransactionAction == send_charged_sms || b.TransactionAction == send_3_first_charged_sms) && b.TransactionDate.Value > startOfMonth);

                    if (monthlyTrans.Any(x => x.Tariff == 0.52m && x.State == "MONTHLY BILL CAP"))
                    {
                        return;
                    }

                    monthlyTrans = monthlyTrans.Where(b => b.State == DELIVRD);
                    //get sum of trans
                    var monthlySum = monthlyTrans.Count() == 0 ? 0 : monthlyTrans.Sum(a => a.Tariff);
                    if (monthlySum >= MONTH_SUM)
                    {
                        return;
                    }

                    //if we still didnt get the first day cap of 6 send the first day msg
                    if (monthlySum < BIG_TARIFF * 6)
                    {
                        SendFirstPaidOfTheMonth(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, 1, p);
                    }
                    else
                    {
                        var dailyTrans = db.UserTransactions.Where(b => b.UserId == u.Id && (b.TransactionAction == send_charged_sms || b.TransactionAction == send_3_first_charged_sms) && b.State == DELIVRD && b.TransactionDate.Value > today);
                        var dailySum = dailyTrans.Count() == 0 ? 0 : dailyTrans.Sum(a => a.Tariff);
                        //what is the big tariff trans count
                        var bigTransCount = monthlyTrans.Count(a => a.Tariff == BIG_TARIFF);
                        bigTransCount = new Random().Next(1, 3);
                        if (dailySum > SMALL_SUM && dailySum < DAILY_SUM)//if we are more the 8* 0.52 we cant send 2.08 we should send 0.52
                        {
                            //send small  small 0.52 sms do not add to count +1 
                            workerlogger.Debug("send small paid sms to user id: {0} with sum:{1}", u.Id, dailySum);
                            SendPaidbatch(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, bigTransCount, p, true);
                        }//send big
                        else if (dailySum < DAILY_SUM)
                        {
                            workerlogger.Debug("send {0} paid sms to user id: {1} with sum:{2}", 3 - monthlyTrans.Count(), u.Id, monthlySum);
                            SendPaidbatch(u.Id, u.PhoneNumber, u.ServiceId, u.AffiliateID, u.Operator, u.PinCode, bigTransCount, p);
                        }
                    }
                }
                catch (Exception ex)
                {
                    workerlogger.Error(ex);
                }
            }

        }

        protected override void OnStop()
        {
            _shutdownEvent.Set();
            if (!_thread.Join(3000))
            { // give the thread 3 seconds to stop
                _thread.Abort();
            }

            if (!_affThread.Join(3000))
            { // give the thread 3 seconds to stop
                _affThread.Abort();
            }

            if (!_moThread.Join(3000))
            { // give the thread 3 seconds to stop
                _moThread.Abort();
            }

            if (!_dlrThread.Join(5000))
            { // give the thread 3 seconds to stop
                _moThread.Abort();
            }
        }

        private void SendffitenDaysRemindeierSMS(int UserId, string Operator, string PhoneNumber, int ServiceId, int? AffiliateID)
        {
            try
            {
                workerlogger.Debug("Send ffiten Days Remindeier SMS to user: {0}", UserId);
                using (var db = new cellEntities())
                {
                    PayDashClient client = new PayDashClient(ServiceId);
                    string seqid = new DAL().GetNextTransactionId();
                    var response = client.Send15DaysUnbilledUsers(PhoneNumber, Operator, ServiceId, seqid);
                    WriteTransaction(db, PhoneNumber, UserId, ServiceId, AffiliateID, client, response, UserTransactionAction.send_fifteen_days_remindeier_sms.ToString(), 0, 0, seqid, "", true);
                    string status = Utility.parseGenericResponse(response);
                    //if send failed remove user
                    if (status != "R0")
                    {
                        var user = db.uspUserStatusUpdate(UserId, (int)UserStatus.unsubscribed, (int)UserDeleteMethod.auto);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }
        }

        private void SendSendMonthlyReminderSMS(int UserId, string Operator, string PhoneNumber, int ServiceId, int? AffiliateID)
        {

            try
            {
                workerlogger.Debug("Send Monthly Reminder SMS to user: {0}", UserId);
                using (var db = new cellEntities())
                {
                    PayDashClient client = new PayDashClient(ServiceId);
                    string seqid = new DAL().GetNextTransactionId();
                    var response = client.SendMonthlyReminder(PhoneNumber, Operator, ServiceId, seqid);
                    WriteTransaction(db, PhoneNumber, UserId, ServiceId, AffiliateID, client, response, UserTransactionAction.send_monthly_reminder_sms.ToString(), 0, 0, seqid, "", true);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }
        }

        private void SendPaidbatch(int UserId, string PhoneNumber, int ServiceId, int? AffiliateID, string Operator, string PinCode, int? sentMsgsCount, Common.Process proc, bool isSmall = false)
        {
            try
            {
                using (var db = new cellEntities())
                {
                    PayDashClient client = new PayDashClient(ServiceId);
                    string seqid = new DAL().GetNextTransactionId();
                    var msg = string.Format(ServicesProvider.GetString(ServiceId, "PaidMessage" + sentMsgsCount), PhoneNumber, PinCode);
                    var response = client.SendPaidSMS(PhoneNumber, Operator, ServiceId, seqid, msg, isSmall);

                    WriteTransaction(db, PhoneNumber, UserId, ServiceId, AffiliateID, client, response, UserTransactionAction.send_charged_sms.ToString(), sentMsgsCount, proc.Id, seqid, msg);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }

        }

        
        private static decimal GetTariff(string tr)
        {
            decimal tariff = 0;
            try
            {
                decimal.TryParse(tr, out tariff);
                if (tariff != 0)
                    tariff = tariff / 100;
            }
            catch (Exception ex)
            {
                workerlogger.Error(ex);
            }

            return tariff;
        }

        public static DateTime GetStartOfWeekDate(DayOfWeek startOfWeek)
        {
            int diff = DateTime.Now.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return DateTime.Now.AddDays(-1 * diff).Date;
        }

        public static void CalculateDailyStat()
        {
            //should be called every day at 23:00
            using (var db = new cellEntities())
            {
                workerlogger.Debug("CalculateDailyStat");
                //get today process ids from process table
                var procs = db.Processes.Where(a => a.StartDate.Year == DateTime.Now.Year && a.StartDate.Month == DateTime.Now.Month && a.StartDate.Day == DateTime.Now.Day).ToList();
                //foreach process id 
                foreach (var proc in procs)
                {
                    var stat = db.uspUserTransactionsDailyStat(proc.Id).FirstOrDefault();
                    //get transactions with process id , calc sum , count and rate 
                    //var deliverdTrans = db.UserTransactions.Where(t => t.ProcessId == proc.Id && t.State == Utility.DELIVRD );
                    //var deliverdTransCount = deliverdTrans.Count();
                    if (stat != null)
                    {
                        var deliverdUserCount = stat.DeliverdUserCount;
                        var sum = stat.Sum;

                        int userCount = proc.UsersCount ?? 0;
                        Decimal rate = 0;
                        if (deliverdUserCount > 0 && userCount > 0)
                            rate = (Decimal)deliverdUserCount / userCount;

                        //update process table
                        proc.ChargedUsersCount = deliverdUserCount;
                        proc.SumCharged = sum;
                        proc.SuccessRate = rate * 100;
                    }

                }

                db.SaveChanges();

            }
        } 
        #endregion
    }
}
