﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SMSEngine
{
    static class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                logger.Info("service started");
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
            {          
                new Service()
            };
                ServiceBase.Run(ServicesToRun);
            }
            catch(Exception ex)
            {
                logger.Fatal(ex);
            }
        }
    }
}
