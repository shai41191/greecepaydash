<?php
/**
 * @package         Asikart.Module
 * @subpackage      mod_example
 * @copyright       Copyright (C) 2012 Asikart.com, Inc. All rights reserved.
 * @license         GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

define('_JEXEC', 1);
define('JPATH_BASE', __DIR__);
define('DS', DIRECTORY_SEPARATOR);

/* Required Files */
require_once(JPATH_BASE . DS . 'includes' . DS . 'defines.php');
require_once(JPATH_BASE . DS . 'includes' . DS . 'framework.php');
$app = JFactory::getApplication('site');
$app->initialise();

require_once(JPATH_BASE . DS . 'components' . DS . 'com_users' . DS . 'models' . DS . 'registration.php');


$action   = JRequest::getVar('action');

if(isset($action))
{
$model = new UsersModelRegistration();

jimport('joomla.mail.helper');
jimport('joomla.user.helper');
$username   = JRequest::getVar('msisdn');
$userId = JUserHelper::getUserId($username);
$instance = JUser::getInstance($userId);

//user should be blocked
	if($action == "delete")
	{
		if(isset($instance))
		{
		$instance->set('block', 1);
		$instance->save();
		$Hello =  $userId. " blocked!";
		}
		else
		{
		$Hello =  "no user blocked!";
		}
	}
	else //if user in system update password and unblock else create
	{
	$password   = JRequest::getVar('code');
		if(isset($userId))
		{
		$instance->set('block', 0);
		$arrpas = array('password' => $password, 'password2' => $password);
		$instance->bind($arrpas);
		$instance->save();
		$Hello =  $userId. " updated!";
		}
		else
		{
		$language = JFactory::getLanguage();
		$language->load('com_users', JPATH_SITE);
		$type       = 0;	
		$name       = JRequest::getVar('msisdn');
		$mobile     = JRequest::getVar('msisdn');
		$email      = JRequest::getVar('msisdn')."@gmail.com";
		$alias      = strtr($name, array(' ' => '-'));
		$sendEmail  = 0;
		$activation = 0;

		$data       = array('username'   => $username,
					'name'       => '*******'.substr($name,-5),
					'email1'     => $email,
					'password1'  => $password, // First password field
					'password2'  => $password, // Confirm password field
					'sendEmail'  => $sendEmail,
					'activation' => $activation,
					'block'      => "0", 
					'mobile'     => $mobile,
					'groups'     => array("2", "10"));
		$response   = $model->register($data);
		}
		if(isset($data['name'])) 
			$Hello =  $data['name']. " saved!";
	}
		
}

require JModuleHelper::getLayoutPath('mod_example', $params->get('layout', 'default'));