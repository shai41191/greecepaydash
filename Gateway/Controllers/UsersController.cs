﻿using Common;
using Common.PayDash;
using Common.PayDashV2;
using Gateway.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Web;
using System.Web.Http;
using System.Xml.Linq;


namespace Gateway.Controllers
{
    public class UsersController : ApiController
    {
        private const string THROTTLING_PREFIX = "User_already_entered_";
        private static Logger logger = LogManager.GetCurrentClassLogger();

        AutoRegisterEngine autoRegEngine = new AutoRegisterEngine();

        public UsersController()
        {

        }
        /*   [Route("Users/ShaiTestlookup")]
            [HttpGet]
            [ExceptionFilter]
            public LookupResult ShaiTestlookup()
            {
                var phoneNumber = "6907006571";
                PayDashClient p = new PayDashClient(1);
                var res = p.Lookup(phoneNumber);
                if (res.Status == "OK" && res.payload.hlrStatus == "SUCCESS" && res.payload.Operator != "NONE")
                {
                    return new LookupResult() { Operator = "GR_" + res.payload.Operator, Status = "success" };

                }
                else
                {
                    if (res.payload.Operator == "NONE")
                    {
                        if (phoneNumber.StartsWith("690") || phoneNumber.StartsWith("693") || phoneNumber.StartsWith("699"))
                        {
                            return new LookupResult() { Operator = "GR_" + "WIND", Status = "success" };
                        }
                        if (phoneNumber.StartsWith("694") || phoneNumber.StartsWith("695"))
                        {
                            return new LookupResult() { Operator = "GR_" + "VODAFONE", Status = "success" };
                        }
                        if (phoneNumber.StartsWith("697") || phoneNumber.StartsWith("698"))
                        {
                            return new LookupResult() { Operator = "GR_" + "COSMOTE", Status = "success" };
                        }
                        return new LookupResult() { Operator = "GR_" + "NONE", Status = "fail" };
                    }
                    else
                        return new LookupResult() { Operator = "GR_" + res.payload.Operator, Status = "fail" };
                    //var re = DIMOCOClient.LookupMsisdn(1, phoneNumber);
                    //return new LookupResult() { Operator = re.Primary, Status = re.Result ? "success" : "fail" };
                }
            }*/

        #region Public

        [Route("Users/CreateUser")]
        [HttpGet]
        [ExceptionFilter]
        public UserResponse CreateUser(string phoneNumber, string affiliateCode = "", string ip = "", int serviceId = 7)
        {

            try
            {
                logger.Info($"CreateUser for phone:{phoneNumber}");
                using (var db = new cellEntities())
                {
                    if (serviceId == 1)
                        serviceId = 7;
                    //AUTO REG
                    if (ip == "0000")
                        return DoAutoReg(db, phoneNumber, affiliateCode, serviceId);

                    logger.Info(this.Request.RequestUri);
                    phoneNumber = Utility.RemovePhonePrefix(phoneNumber);

                    //var aff = db.Affiliates.Where(a => a.Code == affiliateCode).FirstOrDefault();
                    var aff = db.Affiliates.Where(a => a.Code == "Global").FirstOrDefault();//change by shai 07/05/2020

                    if (aff != null && aff.Enabled == false)
                    {
                        logger.Info($"aff not enabled drop requset");
                        return new UserResponse() { Status = true };
                    }

                    int? affid = (aff == null) ? (int?)null : aff.Id;
                    var rand = new Random();

                    
                    

                    var user = GetUser(db, phoneNumber, serviceId);
                   /* if (user != null)
                    {
                        if (user.Operator == "GR_WIND")
                        {
                            return new UserResponse() { Status = false };//add trnslation?
                        }
                    }*/
                    //if the user is unsubscribed and we got diff affid create a new user
                    if (user != null && !(user.Status == (int)UserStatus.unsubscribed && user.AffiliateID != affid))
                    {
                        //if user is entered send pin code again
                        if (user.Status == (int)UserStatus.entered)
                        {
                            if (user.ServiceId == 1)
                            {
                                user.ServiceId = 7;
                                db.SaveChanges();
                            }
                            MemoryCache memoryCache = MemoryCache.Default;
                            logger.Info("User already entered to db send pin agein and return sucess");
                            var u = memoryCache.GetCacheItem(THROTTLING_PREFIX + user.Id.ToString());
                            if (u != null && u.Value != null)
                            {
                                if ((int)u.Value > 4)
                                {
                                    logger.Info("THROTTLING send pin code for user id: " + user.Id);
                                }
                                else
                                {
                                    var item = new CacheItemPolicy();
                                    item.AbsoluteExpiration = DateTimeOffset.Now.AddHours(24);
                                    memoryCache.Set(THROTTLING_PREFIX + user.Id.ToString(), ((int)u.Value) + 1, item);


                                    var res = SendPinCode(db, user);

                                    if (res != "OK" && res != "R0")
                                    {
                                        return new UserResponse() { Status = false, ErrorCode = ErrorCodes.cant_send_pincode, ErrorMessage = res };//add trnslation
                                    }
                                }
                            }
                            else
                            {
                                var item = new CacheItemPolicy();
                                item.AbsoluteExpiration = DateTimeOffset.Now.AddHours(24);
                                memoryCache.Add(THROTTLING_PREFIX + user.Id.ToString(), 2, item);
                                var res = SendPinCode(db, user);
                                if (res != "OK" && res != "R0")
                                {
                                    return new UserResponse() { Status = false, ErrorCode = ErrorCodes.cant_send_pincode, ErrorMessage = res };//add trnslation
                                }
                            }

                        }
                        //if user is active or semibillid send Alerday Subscriber SMS
                        if (user.Status == (int)UserStatus.active || user.Status == (int)UserStatus.semibilled)
                        {
                            logger.Info("User already {0} in db return error", Enum.GetName(typeof(UserStatus), user.Status));
                            //PayDashClient client = new PayDashClient(user.ServiceId);
                            //string seqid = new DAL().GetNextTransactionId();
                            //var response = client.SendAlerdaySubscriberSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);//TODO:write to transaction
                            //WriteTransaction(db, user, seqid, response, UserTransactionAction.send_alerday_subscribed.ToString(), ServicesProvider.GetString(user.ServiceId, "AlreadySubscriberMessage"));
                            //db.SaveChanges();
                            return new UserResponse() { Status = false, ErrorCode = ErrorCodes.user_already_active };
                        }
                        if (user.Status == (int)UserStatus.subscribed)
                        {
                            logger.Info("User already {0} in db return error", Enum.GetName(typeof(UserStatus), user.Status));
                            PayDashClient client = new PayDashClient(user.ServiceId);
                            //string seqid = new DAL().GetNextTransactionId();
                            //var response = client.SendAlerdaySubscriberSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);//TODO:write to transaction
                            //WriteTransaction(db, user, seqid, response, UserTransactionAction.send_alerday_subscribed.ToString(), ServicesProvider.GetString(user.ServiceId, "AlreadySubscriberMessage"));
                            //db.SaveChanges();
                            return new UserResponse() { Status = false, ErrorCode = ErrorCodes.user_already_subscribed };
                        }
                        if (user.Status == (int)UserStatus.unsubscribed || user.Status == (int)UserStatus.blacklist)
                        {
                            logger.Info("User already in db in status unsubscribed", user.Status);
                            user.Status = (int)UserStatus.entered;
                            user.IP = ip;
                            user.AffiliateParams = getUrlReferrerQueryAsJson();
                            user.URL = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.ToString() : "";
                            var res = SendPinCode(db, user);
                            if (res != "OK" && res != "R0")
                            {
                                logger.Info("error sending pin code to:{0}", user.PhoneNumber);
                                return new UserResponse() { Status = false, ErrorCode = ErrorCodes.cant_send_pincode, ErrorMessage = res };//add trnslation
                            }
                        }

                    }
                    else//this is a new user
                    {
                        user = new User();
                        //get operator from lookup
                        var lookup = GetLookup(serviceId, phoneNumber);
                        if (lookup.Status != "success")
                        {
                            logger.Info("error getting lookup fo number:{0} return error in create user", phoneNumber);
                            return new UserResponse() { Status = false, ErrorCode = ErrorCodes.msisdn_lookup };//add trnslation
                        }

                        
                        


                        //block aff 173
                        if((affid == 173 || (affid >= 188 && affid <= 193)) && lookup.Operator == "GR_COSMOTE")
                        {
                            logger.Info("block aff 173");
                            return new UserResponse() { Status = false, ErrorCode = "0" };
                        }

                        user.Operator = lookup.Operator;
                      /*  if(user.Operator== "GR_WIND")
                        {
                            return new UserResponse() { Status = false};//add trnslation?

                        }*/
                        user.AffiliateID = affid;
                        user.IP = ip;
                        user.PinCode = "1111";
                        user.PhoneNumber = phoneNumber; //if we have done lookup this number is valid
                        user.ServiceId = serviceId;
                        user.Status = (int)UserStatus.entered;
                        user.CreationDate = DateTime.Now;
                        user.UpdateDate = DateTime.Now;
                        user.JoinMethod = (int)UserJoinMethod.website;
                        user.AffiliateParams = getUrlReferrerQueryAsJson();
                        user.URL = HttpContext.Current.Request.UrlReferrer != null ? HttpContext.Current.Request.UrlReferrer.ToString() : "";
                        
                       // user.UrlAffCode

                        //add if not exist
                        //int id = db.uspUserCreate(user.ServiceId,user.Status,user.AffiliateID,user.PhoneNumber,user.IP,user.PinCode,user.JoinMethod,user.Operator,user.AffiliateParams,user.URL)
                        int id = new DAL().CreateUser(user);

                        //if the user was really created
                        if (id != 0)
                        {
                            user = db.Users.Where(x => x.Id == id).FirstOrDefault();
                            //send pin code and write the trnsaction
                           // AnalyticsClient.FireEnter(user);
                            //PixelClient.FireEnter(user);
                            var res = SendPinCode(db, user);
                            if (res != "OK" && res != "R0")
                            {
                                logger.Info("error sending pin code to:{0}", user.PhoneNumber);
                                return new UserResponse() { Status = false, ErrorCode = res };//add trnslation?
                            }
                            else
                            {
                                autoRegEngine.AddUser(id, user);
                            }
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                var x = ex.Message.Contains("Subscription session already open");
                if(x==true)//Less than 5 minutes from session start
                {
                    return new UserResponse() { Status = true };

                }
                return new UserResponse() { Status = false, ErrorCode = ErrorCodes.server_error, ErrorMessage = ErrorCodes.server_error };
            }



            return new UserResponse() { Status = true };


        }


        [Route("Users/ValidateUser")]
        [HttpGet]
        [ExceptionFilter]
        public UserResponse ValidateUser(string phoneNumber, int serviceId = 7)
        {
            try
            {
                using (var db = new cellEntities())
                {
                    if (serviceId == 1)
                        serviceId = 7;
                    logger.Info($"ValidateUser phone:{phoneNumber}, serviceid:{serviceId}");

                    phoneNumber = Utility.RemovePhonePrefix(phoneNumber);
                    var user = GetUser(db, phoneNumber, serviceId);
                    if (user != null)
                    {
                        if (user.Status == (int)UserStatus.entered || user.Status == (int)UserStatus.blacklist || user.Status == (int)UserStatus.unsubscribed)
                        {
                            logger.Debug("user sub is not valid , phoneNumber: {0} ", phoneNumber);
                            return new UserResponse() { Status = false, ErrorCode = ErrorCodes.user_pending, ErrorMessage = ErrorCodes.user_pending.ToString() };
                        }
                        else//user active or sub or semi
                        {
                            //PixelClient.SendAffilatePixel(user, false, "subscribe");
                            var pix = PixelClient.SendUrl(user.URL + "&subscribe=1&country=1&phoneNumber=" + user.PhoneNumber, user.Id);//change by shai 10/03/2021
                            logger.Debug($"SendToGlobal Res:{pix}");
                            logger.Debug("user sub is valid , phoneNumber: {0} ", phoneNumber);
                            return new UserResponse() { Status = true };
                        }
                    }
                    else
                    {
                        logger.Debug("user sub is not valid , phoneNumber: {0} ", phoneNumber);
                        return new UserResponse() { Status = false, ErrorCode = ErrorCodes.server_error, ErrorMessage = ErrorCodes.server_error.ToString() };
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new UserResponse() { Status = false, ErrorCode = ErrorCodes.server_error, ErrorMessage = ErrorCodes.server_error };
            }
        }

        [Route("Users/ValidateUserPinCode")]
        [HttpGet]
        [ExceptionFilter]
        public UserResponse ValidateUserPinCode(string phoneNumber, string pinCode, int serviceId =7)
        {
            try
            {
                using (var db = new cellEntities())
                {
                    if (serviceId == 1)
                        serviceId = 7;
                    logger.Info($"ValidateUserPinCode phone:{phoneNumber}, pincode:{pinCode} , serviceid:{serviceId}");
                   // pinCode = "1111";
                    phoneNumber = Utility.RemovePhonePrefix(phoneNumber);
                    var user = GetUser(db, phoneNumber, serviceId);
                    if (user != null)
                    {
                        if (user.Status == (int)UserStatus.entered)
                        {
                            user.Status = (int)UserStatus.subscribed;
                            PayDashClient client = new PayDashClient(user.ServiceId);
                            string seqid = new DAL().GetNextTransactionId();
                           // if (user.Operator == "GR_COSMOTE")
                            //{
                                var res = client.Sub(pinCode.Trim(), user.PinCode, user.UUID, user.ServiceId);
                                logger.Info("send sub response: " + JsonConvert.SerializeObject(res));
                                if (res.response.status == "OK")
                                {
                                    new ServiceProviderClient().CreateUser(user.ServiceId, user.PhoneNumber, user.PinCode);
                                    var response = client.SendWelcomeSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);
                                    user.PinCode = pinCode;
                                    WriteTransaction(db, user, seqid, response, UserTransactionAction.send_welcome_sms.ToString(), ServicesProvider.GetString(user.ServiceId, "WelcomeMessage"));
                                    logger.Info("send Welcome SMS response: " + response);
                                }
                                else
                                {
                                    logger.Debug("user pin code is not valid , phoneNumber: {0} error: {1}", phoneNumber, res.response.error);
                                    return new UserResponse() { Status = false, ErrorCode = ErrorCodes.wrong_pincode, ErrorMessage = ErrorCodes.wrong_pincode.ToString() };
                                }
                            //}
                            //else
                            //{
                            //    if (user.PinCode == pinCode)
                            //    {
                            //        new ServiceProviderClient().CreateUser(user.ServiceId, user.PhoneNumber, user.PinCode);
                            //        var response = client.SendWelcomeSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);
                            //        logger.Info("send Welcome SMS response: " + response);
                            //        WriteTransaction(db, user, seqid, response, UserTransactionAction.send_welcome_sms.ToString());
                                    
                            //    }
                            //    else
                            //    {
                            //        logger.Debug("user pin code is not valid , phoneNumber: {0} ", phoneNumber);
                            //        return new UserResponse() { Status = false, ErrorCode = ErrorCodes.wrong_pincode, ErrorMessage = ErrorCodes.wrong_pincode.ToString() };
                            //    }
                            //}
                            db.SaveChanges();

                            //PixelClient.FireSubscribed(user);
                            //AnalyticsClient.FireSubscribed(user);
                        }
                        logger.Debug("user pin code is valid , phoneNumber: {0} ", phoneNumber);
                        return new UserResponse() { Status = true };
                    }
                    else
                    {
                        logger.Debug("user pin code is not valid , phoneNumber: {0} ", phoneNumber);
                        return new UserResponse() { Status = false, ErrorCode = ErrorCodes.wrong_pincode, ErrorMessage = ErrorCodes.wrong_pincode.ToString() };
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new UserResponse() { Status = false, ErrorCode = ErrorCodes.server_error, ErrorMessage = ErrorCodes.server_error };
            }
        }




        #endregion

        #region private

        private UserResponse DoAutoReg(cellEntities db, string phoneNumber, string affiliateCode, int serviceId)
        {
            logger.Debug("In Auto Reg flow for Number: {0}", phoneNumber);
            int? affid = int.Parse(affiliateCode);//if we are in rereg we got the aff id and not the code 
            var user = db.Users.Where(b => b.PhoneNumber == phoneNumber && b.ServiceId == serviceId && b.AffiliateID == affid).First();
            user.Status = (int)UserStatus.entered;
            user.JoinMethod = (int)UserJoinMethod.auto;
            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(user.AffiliateParams);
            //change values?
            user.UpdateDate = DateTime.Now;

            var res = SendPinCode(db, user);
            if (res != "OK")
            {
                logger.Info("error sending pin code to:{0}", user.PhoneNumber);
                return new UserResponse() { Status = false, ErrorCode = ErrorCodes.cant_send_pincode, ErrorMessage = res };//add trnslation
            }
            else
            {
                //the auto reg engine will contuine the flow
                autoRegEngine.AddUser(user.Id, user);
                return new UserResponse() { Status = true };
            }
        }

        private string getUrlReferrerQueryAsJson()
        {
            string s = string.Empty;
            if (HttpContext.Current.Request.UrlReferrer != null)
            {
                var q = HttpContext.Current.Request.UrlReferrer.ParseQueryString();
                var c = q.Cast<string>().ToDictionary(k => k, v => q[v]);
                s = JsonConvert.SerializeObject(c);
            }
            return s;

        }

        private User GetUserBypPinCode(cellEntities db, string phoneNumber, string pin)
        {
            var user = db.Users.Where(b => b.PhoneNumber == phoneNumber && b.PinCode == pin).FirstOrDefault();
            return user;
        }

        private string SendPinCode(cellEntities db, Common.User user)
        {
            PayDashClientv2 clientv2 = new PayDashClientv2(user.ServiceId);
            var res = clientv2.SubscribeUser(user.PhoneNumber,int.Parse(ServicesProvider.GetString(user.ServiceId, "AppIDHigh")), Utility.GetNetworkId(user.Operator));
            WriteTransaction(db, "0", user, res, UserTransactionAction.send_new_session_sms.ToString());
            db.SaveChanges();

            return res.status;
           // if (user.Operator == "GR_COSMOTE")
            //     SendPinCodeGenPin(db, user);

            //return SendPinCodeOurPin(db, user);
        }

        private string SendPinCodeOurPin(cellEntities db, Common.User user)
        {
            string seqid = new DAL().GetNextTransactionId();
            PayDashClient client = new PayDashClient(user.ServiceId);
            var response = client.SendPinCodeSMS(user.PhoneNumber, user.PinCode, user.Operator, user.ServiceId, seqid);
            logger.Info("send pin code response: " + response);
            WriteTransaction(db, user, seqid, response, UserTransactionAction.send_new_session_sms.ToString(), string.Empty);

            db.SaveChanges();
            return Utility.parseGenericResponse(response);
        }

        private string SendPinCodeGenPin(cellEntities db, Common.User user)
        {
            //string seqid = new DAL().GetNextTransactionId();
            PayDashClient client = new PayDashClient(user.ServiceId);
            var userid = client.GetUsrId(user.PhoneNumber);
            user.UUID = userid;
            var res = client.GenPin(userid, user.ServiceId);
            logger.Info($"gen pin code response for phone:{user.PhoneNumber} " + JsonConvert.SerializeObject(res));
            if (res.response.status == "OK")
            {
                user.PinCode = res.response.id;
            }

            WriteTransaction(db, userid, user, res, UserTransactionAction.send_new_session_sms.ToString());
            db.SaveChanges();

            return res.response.status;
        }

        private void WriteTransaction(cellEntities db, string msgid, User user, GenPinResponse res, string action)
        {

            var trans = new UserTransaction();
            trans.TransactionAction = action;
            trans.Message = string.Empty;
            trans.PhoneNumber = user.PhoneNumber;
            trans.Tariff = 0;
            trans.AffiliateID = user.AffiliateID;
            trans.UserId = user.Id;
            trans.MsgId = msgid;
            trans.ServiceId = user.ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.Operator = user.Operator;
            if (res.response.status == "OK")
            {
                trans.State = UserTransactionState.sent.ToString();
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorDescription = res.response.error;
            }

            db.UserTransactions.AddObject(trans);
        }

        private void WriteTransaction(cellEntities db, string msgid, User user, PayDashResponse res, string action)
        {

            var trans = new UserTransaction();
            trans.TransactionAction = action;
            trans.Message = string.Empty;
            trans.PhoneNumber = user.PhoneNumber;
            trans.Tariff = 0;
            trans.AffiliateID = user.AffiliateID;
            trans.UserId = user.Id;
            trans.MsgId = msgid;
            trans.ServiceId = user.ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.Operator = user.Operator;
            if (res.status == "OK")
            {
                trans.State = UserTransactionState.sent.ToString();
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorDescription = res.messages.FirstOrDefault();
            }

            db.UserTransactions.AddObject(trans);
        }

        private LookupResult GetLookup(int serviceId, string phoneNumber)
        {
            PayDashClient p = new PayDashClient(1);

            var res = p.Lookup(phoneNumber);
 
            if(res==null)
            {
                if (phoneNumber.StartsWith("690") || phoneNumber.StartsWith("693") || phoneNumber.StartsWith("699"))
                {
                    return new LookupResult() { Operator = "GR_" + "WIND", Status = "success" };
                }
                if (phoneNumber.StartsWith("694") || phoneNumber.StartsWith("695"))
                {
                    return new LookupResult() { Operator = "GR_" + "VODAFONE", Status = "success" };
                }
                if (phoneNumber.StartsWith("697") || phoneNumber.StartsWith("698"))
                {
                    return new LookupResult() { Operator = "GR_" + "COSMOTE", Status = "success" };
                }
                return new LookupResult() { Operator = "GR_" + "NONE", Status = "fail" };
            }
            else
            {
                if (res.Status == "OK" && res.payload.hlrStatus == "SUCCESS" && res.payload.Operator != "NONE")
                {
                    return new LookupResult() { Operator = "GR_" + res.payload.Operator, Status = "success" };

                }
                else
                {
                    if (res.payload.Operator == "NONE")
                    {
                        if (phoneNumber.StartsWith("690") || phoneNumber.StartsWith("693") || phoneNumber.StartsWith("699"))
                        {
                            return new LookupResult() { Operator = "GR_" + "WIND", Status = "success" };
                        }
                        if (phoneNumber.StartsWith("694") || phoneNumber.StartsWith("695"))
                        {
                            return new LookupResult() { Operator = "GR_" + "VODAFONE", Status = "success" };
                        }
                        if (phoneNumber.StartsWith("697") || phoneNumber.StartsWith("698"))
                        {
                            return new LookupResult() { Operator = "GR_" + "COSMOTE", Status = "success" };
                        }
                        return new LookupResult() { Operator = "GR_" + "NONE", Status = "fail" };
                    }
                    else
                        return new LookupResult() { Operator = "GR_" + res.payload.Operator, Status = "fail" };


                    //var re = DIMOCOClient.LookupMsisdn(1, phoneNumber);
                    //return new LookupResult() { Operator = re.Primary, Status = re.Result ? "success" : "fail" };
                }

            }

        }

        private void WriteTransaction(cellEntities db, Common.User user, string msgid, string response, string action, string message = "")
        {
            string status = Utility.parseGenericResponse(response);
            var trans = new UserTransaction();
            trans.TransactionAction = action;
            trans.Message = message;
            trans.PhoneNumber = user.PhoneNumber;
            trans.Tariff = 0;
            trans.AffiliateID = user.AffiliateID;
            trans.UserId = user.Id;
            trans.ServiceId = user.ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.MsgId = msgid;
            trans.Operator = user.Operator;
            if (status == "R0")
            {
                trans.State = UserTransactionState.sent.ToString();
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorCode = status;
            }

            db.UserTransactions.AddObject(trans);

        }

        private User GetUser(cellEntities db, string phoneNumber, int serviceId)
        {

            var users = db.Users.Where(b => b.PhoneNumber == phoneNumber && b.ServiceId == serviceId);
            if (users.Count() == 0)
            {
                if (serviceId == 7)
                {
                    users = db.Users.Where(b => b.PhoneNumber == phoneNumber && b.ServiceId == 1);
                    if (users.Count() == 0)
                        return null;
                }
                else return null;
            }
                //return null;

            return users.ToList().Last();
        }
        #endregion


    }
}
