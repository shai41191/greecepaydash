﻿using Common;
using Common.PayDash;
using Common.PayDashV2;
using Gateway.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace Gateway.Controllers
{
    public class CallBackController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        const decimal BigTariff = 2.08m;
        const decimal SmallTariff = 0.52m;
        const decimal WEEKLY_SUM = BigTariff * 3;
        const decimal MONTHLY_SUM = WEEKLY_SUM * 4;
        const decimal FIRST_DAY_SUM = BigTariff * 6;
        const decimal EVERY_DAY_SUM = BigTariff * 3;
        private const string OUT_OF_CREDIT = "OUT_OF_CREDIT";
        private const string MONTHLY_BILL_CAP = "MONTHLY BILL CAP";

        public CallBackController()
        {

        }

        #region Public
        [Route("CallBack/DLR")]
        [HttpPost]
        [ExceptionFilter]
        public BaseResponse DeliveryNotification([FromBody]PYReq req)
        {
            logger.Info("DeliveryNotification request: {0}", this.Request.RequestUri);
            try
            {
                RedisClient c = new RedisClient();
                c.SaveDLRXml(req.TP_XML);
                logger.Info($"DLR writen to redis:{req.TP_XML}");
            }
            catch(Exception ex)
            {
                logger.Fatal(ex);
            }
            return new BaseResponse() { Status = true };
        }


        [Route("CallBack/IVR/Unsubscribe")]
        [HttpGet]
        [ExceptionFilter]
        public BaseResponse Unsubscribe(string MSISDN, string Operator = "")
        {
            logger.Info(this.Request.RequestUri);

            using (var db = new cellEntities())
            {
                MSISDN = Utility.RemovePhonePrefix(MSISDN);

                var users = GetUserToRemove(MSISDN, "STOP", db);
                if (users.Count() == 0)
                {
                    logger.Error("Could not find the user by phone number:{0} for MO", MSISDN);
                    return new BaseResponse() { Status = false };
                }

                foreach (var user in users)
                {
                    if (user.Status == (int)UserStatus.blacklist || user.Status == (int)UserStatus.unsubscribed || user.Status == (int)UserStatus.entered)
                        continue;

                    var tran = new UserTransaction();
                    tran.UserId = user.Id;
                    tran.ServiceId = user.ServiceId;
                    tran.PhoneNumber = MSISDN;
                    tran.TransactionDate = DateTime.Now;
                    tran.Operator = Operator;
                    tran.Message = string.Empty;
                    tran.Udh = string.Empty;
                    //tran.Signifier = signifier;
                    tran.TransactionAction = "IVR/Unsubscribe";
                    db.UserTransactions.AddObject(tran);
                    db.SaveChanges();
                    //if (KEYWORD  STOP) remove user , we should decativate him

                    logger.Debug("IVR cell stop user:{0} with phone number {1}", user.Id, MSISDN);
                    user.Status = (int)UserStatus.unsubscribed;
                    user.DeleteMethod = (int)UserDeleteMethod.ivr;
                    SendunsbscriptionSMS(db, user);
                    //no need to save to db its in the write transaction
                    logger.Info("user {0} was unsubscribed", user.Id);

                }
            }
            return new BaseResponse() { Status = true };
        }

        [Route("CallBack/MO")]
        [HttpPost]
        [ExceptionFilter]
        public BaseResponse MoForwarding([FromBody]PYReq req)
        {

            logger.Info("MoForwarding request: {0}", this.Request.RequestUri);
            try
            {
                RedisClient c = new RedisClient();
                c.SaveMOXml(req.TP_XML);
                logger.Info($"MO writen to redis:{req.TP_XML}");
            }
            catch (Exception ex)
            {
                logger.Fatal(ex);
            }
            return new BaseResponse() { Status = true };

        }
        

        #endregion


        #region private


        private static List<Common.User> GetUserToRemove(string from, string keyword, cellEntities db)
        {
            List<User> users = new List<User>();
            if (keyword.ToUpper().Contains("STOP"))
            {
                users = db.Users.Where(a => a.PhoneNumber == from).ToList();
            }
            //else
            //{
            //    var service = db.Services.Where(s => s.ServiceName.Equals(keyword, StringComparison.InvariantCultureIgnoreCase)).First();
            //    users = db.Users.Where(a => a.PhoneNumber == from && a.ServiceId == service.Id).ToList();
            //}
            return users;
        }

        private void SendunsbscriptionSMS(cellEntities db, Common.User user)
        {
            string seqid = new DAL().GetNextTransactionId();
            PayDashClient c = new PayDashClient(user.ServiceId);
            var res = c.SendUnsubscriptionSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);
            WriteTransaction(db, user, c, seqid, res, UserTransactionAction.send_termination_of_subscription.ToString(), ServicesProvider.GetString(user.ServiceId, "UnsubscribedMessage"));
        }

        private void WriteTransaction(cellEntities db, Common.User user, PayDashClient client, string msgid, string response, string action,string msg, int count = 0, int? ProcessId = 0, bool isFree = true, bool isSmall =false)
        {
            string status = Utility.parseGenericResponse(response);
            var tariff = isSmall == true ? SmallTariff : BigTariff;
            var trans = new UserTransaction();
            trans.TransactionAction = action;
            trans.Tariff = isFree ? 0 : tariff; 
            trans.PhoneNumber = user.PhoneNumber;
            trans.Operator = user.Operator;
            trans.Message = msg;
            //if (decimal.TryParse(client.webclient.QueryString[DIMOCOClient.TARIFF], out tariff))
            //{
            //    if (tariff != 0)
            //        trans.Tariff = tariff/100;
            //}
            trans.UserId = user.Id;
            trans.ProcessId = ProcessId;
            trans.AffiliateID = user.AffiliateID;
            trans.ServiceId = user.ServiceId;
            trans.TransactionDate = DateTime.Now;
            trans.MsgId = msgid;
            trans.Signifier = count.ToString();
            if (status == "R0")
            {
                trans.State = UserTransactionState.sent.ToString(); ;
            }
            else
            {
                trans.State = UserTransactionState.failed.ToString();
                trans.ErrorCode = status;
            }

            db.UserTransactions.AddObject(trans);
            db.SaveChanges();
        }
         #endregion

    }
}
