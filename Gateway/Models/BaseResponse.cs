﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gateway.Models
{
    public class BaseResponse
    {
        public bool Status { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCode { get; set; } 
        
    }
}