﻿using Common;
using Gateway.Controllers;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Caching;
using System.Web;



namespace Gateway
{
    public class AutoRegisterEngine
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void AddUser(int id, Common.User user)
        {
            if (user.Operator == "GR_COSMOTE")//GR_COSMOTE does not support autoreg
                return;

            logger.Debug("In Add user to AutoRegisterEngine , user id:{0}", id);
            try
            {
            using (var db = new cellEntities())
            {

                TimeSpan start = new TimeSpan(int.Parse(ConfigurationManager.AppSettings["DaliyStartAutoRegisterHour"]), 0, 0);
                TimeSpan end = new TimeSpan(int.Parse(ConfigurationManager.AppSettings["DailyEndAutoRegisterHour"]), 0, 0);

                TimeSpan weekendStart = new TimeSpan(int.Parse(ConfigurationManager.AppSettings["WeekEndStartAutoRegisterHour"]), 0, 0);
                TimeSpan weekendEnd = new TimeSpan(int.Parse(ConfigurationManager.AppSettings["WeekEndEndAutoRegisterHour"]), 0, 0); 
                bool isWeekEndEndAutoRegisterEnabled = bool.Parse(ConfigurationManager.AppSettings["WeekEndEndAutoRegisterEnabled"]);

                if (isWeekEndEndAutoRegisterEnabled && IsWeekend())
                {
                    start = weekendStart;
                    end = weekendEnd;
                }

                //validate its in time range and hass aff data and no trnsactions
                if (TimeBetween(DateTime.Now, start, end) && user.AffiliateID != null )
                {
                    var service = db.Services.Where(s => s.Id == user.ServiceId).First();
                    //service is on and user ip not in black list

                    if (service.EnableAutoRegister && IsValidIP(service,user.IP) && service.AutoRegisterMaxCount.Value > service.AutoRegisterCount.Value)
                    {
                        Random rnd = new Random();
                        var secs = rnd.Next(125, 180);
                        MemoryCache memoryCache = MemoryCache.Default;
                        var item = new CacheItemPolicy();
                        item.AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(secs);
                        item.RemovedCallback = this.OnRemoveFromCache;
                        memoryCache.Add(id.ToString(), user, item);
                        logger.Debug("user add to callback ,user will callback in {0} secs for user id:{1}",secs, id);
                    }
                }
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
            
            
            
        }

        private bool IsWeekend()
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday || DateTime.Now.DayOfWeek == DayOfWeek.Sunday )
                return true;

            return false;
        }

        public static bool IsHasNoTransactions(Common.User user)
        {
            var trans = user.UserTransactions;
            bool b = false;
            if (trans.Count() == 1 && trans.First().State == Utility.DELIVRD)
                b = true;

            logger.Debug("IsHasNoTransactions for user: {0} with value : {1}", user.Id,b);
            
            return b;
        }

        bool TimeBetween(DateTime datetime, TimeSpan start, TimeSpan end)
        {
            TimeSpan now = datetime.TimeOfDay;

            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
            
        }

        public static bool IsVaidAff(User user)
        {
            bool isvalid = true;

            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(user.AffiliateParams);
            Affiliate aff = null;

            aff = user.Affiliate;
            
             
            var p = aff.param1;
            if (!string.IsNullOrEmpty(p))
            {
                if(!values.ContainsKey(p))
                    isvalid = false;
            }

            p = aff.param2;
            if (!string.IsNullOrEmpty(p))
            {
                if (!values.ContainsKey(p))
                    isvalid = false;
            }

            p = aff.param3;
            if (!string.IsNullOrEmpty(p))
            {
                if (!values.ContainsKey(p))
                    isvalid = false;
            }

            p = aff.param4;
            if (!string.IsNullOrEmpty(p))
            {
                if (!values.ContainsKey(p))
                    isvalid = false;
            }

            

            logger.Debug("IsVaidAff for user: {0} with value: {1}", user.Id, isvalid);
            return isvalid;
        }

        public static bool IsValidIP(Service service ,string IP)
        {
            var isValidIp = true;
            if(service.AutoRegisterIPBlackList != null)
            {
                isValidIp = !service.AutoRegisterIPBlackList.Split(',').Contains(IP);
            }
            logger.Debug("IsValidIP for IP :{0} returned value :{1}",IP,isValidIp);
            return isValidIp;
        }

        public void OnRemoveFromCache(CacheEntryRemovedArguments arg)
        {
            try
            {
                var user = (User)arg.CacheItem.Value;
                logger.Debug("OnRemoveFromCache for user id:{0}", user.Id);
                using (var db = new cellEntities())
                {

                    var newUser = db.Users.Where(uu => uu.Id == user.Id).FirstOrDefault();

                    if ((newUser.JoinMethod == (int)UserJoinMethod.auto && newUser.UserTransactions.OrderByDescending(a => a.TransactionDate).First().State == Utility.DELIVRD) || (newUser.Status == (int)UserStatus.entered && IsVaidAff(newUser) && IsHasNoTransactions(newUser) && NotFromKnownIP(newUser,db)))
                    {
                        var con = new UsersController();
                        con.Request = new System.Net.Http.HttpRequestMessage();
                        con.ValidateUserPinCode(user.PhoneNumber, user.PinCode,user.ServiceId);
                        newUser.Service.AutoRegisterCount = newUser.Service.AutoRegisterCount + 1;
                        db.SaveChanges();
                        logger.Debug("User id :{0} was autoregistered", user.Id);
                    }


                }
            }
            catch(Exception ex)
            {
                logger.Error(ex);
            }
        }

        public static bool NotFromKnownIP(User newUser, cellEntities db)
        {
            //bypass for this aff ids
            int?[] idLst = { 164, 173, 192, 188, 189, 190, 191, 192, 193, 160 };
            if (idLst.ToList().Contains(newUser.AffiliateID))
                return true;

            var res = true;
            var date = DateTime.Now.AddDays(-1);
            var users = db.Users.Where(u => u.IP == newUser.IP && u.Id != newUser.Id && u.ServiceId == newUser.ServiceId && u.CreationDate > date);
            if (users.Count() > 0)
                res = false;

            logger.Debug("NotFromKnownIP return value: {0} ,with ip: {1}", res,newUser.IP);
            return res;
        }

        
    }
}