﻿using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Gateway
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override void OnException(HttpActionExecutedContext context)
        {
            logger.Error($"endpoint: {context.Request.RequestUri.AbsolutePath} ,Exception: {context.Exception} , context: {context.Request.Content.ReadAsStringAsync().Result}");
           context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError); 
        }
    }
}