﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;

namespace Gateway
{
    public class MemoryCacher
    {
        public object GetValue(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Get(key);
        }

 

        public void Set(string key, object value, DateTimeOffset absExpiration)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            memoryCache.Set(key, value, absExpiration);
        }

        public void Delete(string key)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
        }

        internal bool AddUser(int id, Common.User user, DateTimeOffset dateTimeOffset, CacheEntryRemovedCallback cacheEntryRemovedCallback)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            var item = new CacheItemPolicy();
            item.AbsoluteExpiration = dateTimeOffset;
            item.RemovedCallback = cacheEntryRemovedCallback;
            return memoryCache.Add(id.ToString(), user, item);
        }
    }
}