﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Data.Entity;
using System.Linq;
using System.Web.Security;
using System.Web;

namespace BackOffice
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(HttpContext.Current.User.Identity.Name == "cclview")
            {
                Response.Redirect("CCLView.aspx");
            }

            System.Collections.IList visibleTables = Global.DefaultModel.VisibleTables;
            if (visibleTables.Count == 0)
            {
                throw new InvalidOperationException("There are no accessible tables. Make sure that at least one data model is registered in Global.asax and scaffolding is enabled or implement custom pages.");
            }
            Menu1.DataSource = visibleTables;
            Menu1.DataBind();

            PopulateCharge();
        }

        private void PopulateCharge()
        {
            var transactionOptions = new System.Transactions.TransactionOptions();
            //set it to read uncommited
            transactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;
            //create the transaction scope, passing our options in
            using (var transactionScope = new System.Transactions.TransactionScope(
                System.Transactions.TransactionScopeOption.Required,
                transactionOptions)
            )
            {
                using (var context = new Common.cellEntities())
                {
                    context.CommandTimeout = 180;
                    var todayCharge = context.UserTransactions.Where(a => a.TransactionDate >= DateTime.Today && a.State == Common.Utility.DELIVRD).Sum(s => s.Tariff);
                    Label1.Text = todayCharge.HasValue ? todayCharge.ToString() : "0.00";

                    var now = DateTime.Now;
                    var startOfMonth = new DateTime(now.Year, now.Month, 1);
                    var monCharge = context.UserTransactions.Where(a => a.TransactionDate >= startOfMonth && a.State == Common.Utility.DELIVRD).Sum(s => s.Tariff);
                    Label2.Text = monCharge.HasValue ? monCharge.ToString() : "0.00";
                    transactionScope.Complete();
                }
            }


        }

    }
}
