﻿using Common;
using Common.PayDash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BackOffice
{
    public partial class CCLView : System.Web.UI.Page
    {

        private string send_3_first_charged_sms1;
        private string send_new_session_sms;

        public string send_welcome_sms { get; private set; }

        private string send_termination_of_subscription;
        private string send_charged_sms1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                using (var context = new Common.cellEntities())
                {
                    var us = context.Users.Where(x => x.PhoneNumber == userPhone.Text).FirstOrDefault();
                    send_3_first_charged_sms1 = ServicesProvider.GetString(us.ServiceId, "FirstPaidMessage1");
                    send_new_session_sms = ServicesProvider.GetString(us.ServiceId, "PincodeMessage");
                    send_welcome_sms = ServicesProvider.GetString(us.ServiceId, "WelcomeMessage");
                    send_termination_of_subscription = ServicesProvider.GetString(us.ServiceId, "UnsubscribedMessage");
                    send_charged_sms1 = ServicesProvider.GetString(us.ServiceId, "PaidMessage1");
                }
            }

            
        }




        private void Bind(cellEntities context)
        {
            var users = (from user in context.Users
                         where user.PhoneNumber == userPhone.Text
                         select new
                         {
                             user.Id,
                             KEYWORD = user.Service.ServiceName,
                             Status =
                               (
                                   user.Status == (int)UserStatus.entered ? "entered" :
                                   user.Status == (int)UserStatus.active ? "active" :
                                   user.Status == (int)UserStatus.semibilled ? "semibilled" :
                                   user.Status == (int)UserStatus.subscribed ? "subscribed" :
                                   user.Status == (int)UserStatus.unsubscribed ? "unsubscribed" :
                                   user.Status == (int)UserStatus.blacklist ? "blacklist" : "Unknown"
                               ),
                             user.PhoneNumber,
                             user.CreationDate,
                             UNSUBSCRIBED_DATE =
                             (
                             user.Status == (int)UserStatus.unsubscribed ? user.UpdateDate : new DateTime()
                             ),
                             user.IP,
                             user.PinCode,
                             DeleteMethod =
                             (
                                 user.DeleteMethod == (int)UserDeleteMethod.auto ? "auto" :
                                 user.DeleteMethod == (int)UserDeleteMethod.ivr ? "ivr" :
                                 user.DeleteMethod == (int)UserDeleteMethod.manual ? "manual" :
                                 user.DeleteMethod == (int)UserDeleteMethod.sms ? "sms" :
                                 ""
                             ),
                             user.Balance,
                             user.Operator,
                             user.URL,
                             user.SentCount,


                         }
                                             );

            GridView1.DataSource = users;
            GridView1.DataBind();

            var us = users.Select(x => x.Id);

            if (us != null)
            {
                //var q = context.uspUserGetAllLive();
                var tt = (from tran in context.UserTransactions
                          where us.Contains(tran.UserId)
                          select new
                          {
                              tran.Id,
                              tran.TransactionDate,
                              tran.DLRDate,
                              tran.TransactionAction,
                              tran.UserId,
                              tran.State,
                              Message =
                              (
                              (tran.Message.Trim() == "" && tran.TransactionAction == "send_termination_of_subscription") ? send_termination_of_subscription :
                              (tran.Message.Trim() == "" && tran.TransactionAction == "send_3_first_charged_sms") ? send_3_first_charged_sms1 :
                              (tran.Message.Trim() == "" && tran.TransactionAction == "send_welcome_sms") ? send_welcome_sms :
                              (tran.Message.Trim() == "" && tran.TransactionAction == "send_new_session_sms") ? send_new_session_sms :
                              (tran.Message.Trim() == "" && tran.TransactionAction == "send_charged_sms") ? send_charged_sms1 :
                                 tran.Message
                              ),
                              tran.PhoneNumber,
                              tran.Tariff,
                              tran.MsgId,
                              tran.ErrorCode,
                              tran.ErrorDescription,

                          }

                ).OrderByDescending(c => c.TransactionDate);
               
                GridView2.DataSource = tt;// context.UserTransactions.Where(u => u.UserId == us.Id);
                GridView2.DataBind();
            }
        }

        private string GetMsg(string transactionAction, string signifier, int serviceId)
        {
            return ServicesProvider.GetString(serviceId, "PaidMessage1");
        }

        protected void Export_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=GridViewExport.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            using (var context = new Common.cellEntities())
            {
                var us = context.Users.Where(x => x.PhoneNumber == userPhone.Text).FirstOrDefault();

                if (us == null)
                    return;

                //var q = context.uspUserGetAllLive();
                var tt = (from tran in context.UserTransactions
                          where tran.UserId == us.Id
                          select new
                          {
                              tran.Id,
                              tran.TransactionDate,
                              tran.DLRDate,
                              tran.TransactionAction,
                              tran.UserId,
                              tran.State,
                              tran.Message,
                              tran.PhoneNumber,
                              tran.Tariff,
                              tran.MsgId,
                              tran.ErrorCode,
                              tran.ErrorDescription,
                              tran.Signifier,
                              tran.ProcessId
                          }

                ).OrderByDescending(c => c.TransactionDate);



                var sb = ConvertLINQResultsToCSV(tt, ",");



                Response.Output.Write(sb);
                Response.Flush();
                Response.End();
            }
        }

        private static string ConvertLINQResultsToCSV(IQueryable query, string replacementDelimiter)
        {

            // Create the csv by looping through each row and then each field in each row
            // seperating the columns by commas

            // String builder for our header row
            StringBuilder header = new StringBuilder();

            // Get the properties (aka columns) to set in the header row
            PropertyInfo[] rowPropertyInfos = null;
            rowPropertyInfos = query.ElementType.GetProperties();

            // Setup header row 
            foreach (PropertyInfo info in rowPropertyInfos)
            {
                if (info.CanRead)
                {
                    header.Append(info.Name + ",");
                }
            }

            // New row
            header.Append("\r\n");

            // String builder for our data rows
            StringBuilder data = new StringBuilder();

            // Setup data rows
            foreach (var myObject in query)
            {

                // Loop through fields in each row seperating each by commas and replacing 
                // any commas in each field name with replacement delimiter
                foreach (PropertyInfo info in rowPropertyInfos)
                {
                    if (info.CanRead)
                    {

                        // Get the fields value and then replace any commas with the replacement delimeter
                        string tmp = Convert.ToString(info.GetValue(myObject, null));
                        if (!String.IsNullOrEmpty(tmp))
                        {
                            tmp.Replace(",", replacementDelimiter);
                        }
                        data.Append(tmp + ",");
                    }
                }

                // New row
                data.Append("\r\n");
            }

            // Check the data results... if they are empty then return an empty string
            // otherwise append the data to the header
            string result = data.ToString();
            if (string.IsNullOrEmpty(result) == false)
            {
                header.Append(result);
                return header.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "Unsubscribe") return;
            int userid = Convert.ToInt32(e.CommandArgument);
            using (var context = new Common.cellEntities())
            {
                var user = context.Users.Where(x => x.Id == userid).First();
                new ServiceProviderClient().DeleteUser(user.ServiceId, user.PhoneNumber);
                context.uspUserStatusUpdate(user.Id, (int)UserStatus.unsubscribed, (int)UserDeleteMethod.manual);
                PayDashClient c = new PayDashClient(user.ServiceId);
                string seqid = new DAL().GetNextTransactionId();
                var res = c.SendUnsubscriptionSMS(user.PhoneNumber, user.Operator, user.ServiceId, seqid);
                var trans = new UserTransaction();
                trans.TransactionAction = UserTransactionAction.send_termination_of_subscription.ToString();
                trans.PhoneNumber = user.PhoneNumber;
                trans.Message = "";
                trans.MsgId = seqid;
                trans.UserId = user.Id;
                trans.ServiceId = user.ServiceId;
                trans.TransactionDate = DateTime.Now;
                trans.State = UserTransactionState.sent.ToString();
                trans.ErrorDescription = "Unsubscribe from CCLVIEW";

                context.UserTransactions.AddObject(trans);
                context.SaveChanges();
                Bind(context);
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(userPhone.Text))
            {
                using (var context = new Common.cellEntities())
                {
                    Bind(context);
                }
            }
        }

        public bool IsActive(object Status)
        {
            var ss = new[] { "active", "semibilled", "subscribed" };
            if (ss.Any(x => x == Status.ToString()))
                return true;


            return false;
        }
    }
}