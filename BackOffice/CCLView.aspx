﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CCLView.aspx.cs" Inherits="BackOffice.CCLView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>paydash CCL View</title>
    <link href="~/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Paydash User details and transactions</h1>

<asp:textbox ID="userPhone" runat="server"></asp:textbox><asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" />
<br />
<br />
       <h3> User details:</h3>
         <asp:GridView ID="GridView1" PageSize="50" runat="server"  EnablePersistedSelection="true"
                AllowPaging="True" AllowSorting="False" CssClass="DDGridView"
                RowStyle-CssClass="td" HeaderStyle-CssClass="th" CellPadding="6" DataKeyNames="Id" OnRowCommand="GridView1_RowCommand">

                 <Columns>
            <asp:TemplateField ShowHeader="False">
            <ItemTemplate>
               
                <asp:Button ID="Button1" runat="server" CausesValidation="false" Visible='<%# IsActive(Eval("Status")) %>' CommandName="Unsubscribe"
                    Text="Unsubscribe" CommandArgument='<%# Eval("Id") %>'  />
                   
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
            </asp:GridView>
        <br />
        <asp:Button ID="Export" runat="server" Text="Export" OnClick="Export_Click" />
<br />
       <h3>User transactions:</h3> 
        <br />
        <asp:GridView ID="GridView2"  runat="server"  EnablePersistedSelection="true"
                AllowPaging="False" AllowSorting="True" CssClass="DDGridView"
                RowStyle-CssClass="td" HeaderStyle-CssClass="th" CellPadding="6" DataKeyNames="Id">

                <EmptyDataTemplate>
                    There are currently no items in this table.
                </EmptyDataTemplate>
            </asp:GridView>


    </div>
    </form>
</body>
</html>
