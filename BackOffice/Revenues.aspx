﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Revenues.aspx.cs" Inherits="BackOffice.Revenues" %>

<asp:Content ID="headContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server" />

    <h2 class="DDSubHeader">Revenue Page</h2>

    
    <div>
    <asp:TextBox 
    ID="txbDateFrom" 
    runat="server" 
    CssClass="DDFilter">
</asp:TextBox>
<ajaxToolkit:CalendarExtender 
    ID="txbDateFrom_CalendarExtender" 
    runat="server" 
    Enabled="True" 
    TargetControlID="txbDateFrom">
</ajaxToolkit:CalendarExtender>
to
<asp:TextBox 
    ID="txbDateTo" 
    runat="server" 
    CssClass="DDFilter">
</asp:TextBox>
<ajaxToolkit:CalendarExtender 
    ID="txbDateTo_CalendarExtender" 
    runat="server" 
    Enabled="True" 
    TargetControlID="txbDateTo">
</ajaxToolkit:CalendarExtender>
<asp:Button 
    ID="btnRangeButton" 
    CssClass="DDFilter"
    runat="server" 
    Text="Select" 
    OnClick="btnRangeButton_Click" />
<asp:Button 
    ID="btnClear" 
    CssClass="DDFilter"
    runat="server" 
    Text="Clear" 
    OnClick="btnRangeButton_Click" />
</br>
    </div>
</br>

     Total Revenue:
    <asp:Label runat="server" ID="Label1"></asp:Label><br />

</asp:Content>
