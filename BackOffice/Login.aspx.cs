﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BackOffice
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Logon_Click(object sender, EventArgs e)
        {
            if ((UserEmail.Text == "ami9620@gmail.com") && (UserPass.Text == "9620"))
            {
                FormsAuthentication.RedirectFromLoginPage(UserEmail.Text, true);
            }
            else if ((UserEmail.Text == "cclview") && (UserPass.Text == "4301"))
            {
                FormsAuthentication.SetAuthCookie(UserEmail.Text, true);
                Response.Redirect("CCLView.aspx");
            }
            else
            {
                Msg.Text = "Invalid credentials. Please try again.";
            }
        }
    }
}