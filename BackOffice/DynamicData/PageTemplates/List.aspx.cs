﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Text;
using System.Collections;
using Common;


namespace BackOffice
{
    public partial class List : System.Web.UI.Page
    {
        protected MetaTable table;

        protected void Page_Init(object sender, EventArgs e)
        {
            table = DynamicDataRouteHandler.GetRequestMetaTable(Context);
            GridView1.SetMetaTable(table, table.GetColumnValuesFromRoute(Context));
            GridDataSource.EntityTypeFilter = table.EntityType.Name;
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Title = table.DisplayName;
            GridDataSource.Include = table.ForeignKeyColumnsNames;

            //Dan code
            //Control date_filter = FilterRepeater.Controls[1];
            //FilterRepeater.Controls.RemoveAt(1);
            //FilterRepeater.Controls.Add(date_filter); 

            // Disable various options if the table is readonly
            if (table.IsReadOnly)
            {
                GridView1.Columns[0].Visible = false;
                InsertHyperLink.Visible = false;
                GridView1.EnablePersistedSelection = false;
            }
            string join = string.Empty;
            string  delete = string.Empty;
            string action = string.Empty; 

            if(table.Name.Equals("users",StringComparison.CurrentCultureIgnoreCase))
            {
                var values = Enum.GetValues(typeof(Common.UserJoinMethod));
                join = "JoinMethod- ";
                for(int i=0; i < values.Length; i++)
                {
                    join += i+1 +":"+ values.GetValue(i)+" ";
                }

                var values1 = Enum.GetValues(typeof(Common.UserDeleteMethod));
                delete = "DeleteMethod: ";
                for (int i = 0; i < values1.Length; i++)
                {
                    delete += i+1 + ":" + values1.GetValue(i) + " ";
                }
                lblEnums.Text = join + "<br/> " + delete;
                
            }

            if (table.Name.Equals("UserTransactions", StringComparison.CurrentCultureIgnoreCase))
            {
                var values = Enum.GetValues(typeof(Common.UserTransactionAction));
                action = "UserTransactionAction: ";
                for (int i = 0; i < values.Length; i++)
                {
                    action +=  values.GetValue(i) + ", ";
                }
                lblEnums.Font.Size = FontUnit.Small;
                lblEnums.Text = action;
            }

            

            if (!IsPostBack)
            {
                if (table.DisplayName == "Users")
                    GridView1.Sort("CreationDate", SortDirection.Descending);

                if (table.DisplayName == "UserTransactions")
                    GridView1.Sort("TransactionDate", SortDirection.Descending);

                if (table.DisplayName == "AffiliateTransactions")
                    GridView1.Sort("TransactionDate", SortDirection.Descending);

                if (table.DisplayName == "Processes")
                    GridView1.Sort("StartDate", SortDirection.Descending);
            }
        }

        protected void Label_PreRender(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            DynamicFilter dynamicFilter = (DynamicFilter)label.FindControl("DynamicFilter");
            QueryableFilterUserControl fuc = dynamicFilter.FilterTemplate as QueryableFilterUserControl;
            if (fuc != null && fuc.FilterControl != null)
            {
                label.AssociatedControlID = fuc.FilterControl.GetUniqueIDRelativeTo(label);
            }
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary(GridView1.GetDefaultValues());
            InsertHyperLink.NavigateUrl = table.GetActionPath(PageAction.Insert, routeValues);
            base.OnPreRenderComplete(e);
            //var t = GridDataSource;
            
        }

        protected void GridDataSource_Selected(object sender, EntityDataSourceSelectedEventArgs e)
        {
            
            lblPageCount.Text  = String.Concat("Total Records: ", e.SelectArguments.TotalRowCount);
            var t = e.Results;
            Session["Results"] = t;
        }

        protected void DynamicFilter_FilterChanged(object sender, EventArgs e)
        {
            GridView1.PageIndex = 0;
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {

            var results = Session["Results"];
            IEnumerable lst = ((IEnumerable)results);
            string attachment = "attachment; filename=" + table.DisplayName+".csv";
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");

            var sb = new StringBuilder();
            string head = string.Empty;
            foreach(var c in table.Columns)
            {
               if(c.GetType() == typeof(MetaColumn))
               {
                    head += c.Name;
                    head += ",";
               }
                else if(c.GetType() == typeof(System.Web.DynamicData.MetaForeignKeyColumn) && table.DisplayName.Equals("AffiliateTransactions",StringComparison.CurrentCultureIgnoreCase))
                {
                    if (c.Name == "Affiliate")
                    {
                        head += c.Name;
                        head += ",";
                    }
                }
            }
            sb.AppendLine(head);
            foreach (var obj in lst)
                sb.AppendLine(TransformDataLineIntoCsv(obj));

            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }



        private string TransformDataLineIntoCsv(object obj)
        {
            string line = string.Empty;


            foreach (var c in table.Columns)
            {
                if (c.GetType() == typeof(MetaColumn))
                {
                    var cell = obj.GetType().GetProperty(c.Name).GetValue(obj, null);
                    //cast user status to string from enum
                    if (obj.GetType() == typeof(User) && c.Name == "Status")
                    {
                        int id = (int)cell;
                        string val = ((UserStatus)id).ToString();
                        line += val;
                        line += ",";
                    }
                    else
                    {
                    line += cell != null ? cell.ToString() : "";
                    line += ",";
                    }
                }
                if(c.GetType() == typeof(System.Web.DynamicData.MetaForeignKeyColumn))
                {
                    if (obj.GetType().GetProperty(c.Name).GetValue(obj, null) != null && obj.GetType().GetProperty(c.Name).GetValue(obj, null).GetType() == typeof(Affiliate))
                    {
                        var cell = (Affiliate)obj.GetType().GetProperty(c.Name).GetValue(obj, null);
                        line += cell != null ? cell.Code : "";
                        line += ",";
                    }
                }
            }
            return line;
        }

        protected void btnExportAll_Click(object sender, EventArgs e)
        {
            GridView1.AllowPaging = false;
            GridView1.DataBind();
            var results = Session["Results"];
            IEnumerable lst = ((IEnumerable)results);
            string attachment = "attachment; filename=" + table.DisplayName + ".csv";
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.AddHeader("content-disposition", attachment);
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("Pragma", "public");

            var sb = new StringBuilder();
            string head = string.Empty;
            foreach (var c in table.Columns)
            {
                if (c.GetType() == typeof(MetaColumn))
                {
                    head += c.Name;
                    head += ",";
                }
            }
            sb.AppendLine(head);
            foreach (var obj in lst)
                sb.AppendLine(TransformDataLineIntoCsv(obj));

            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }

    }
}
