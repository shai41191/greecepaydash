﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Web.DynamicData;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BackOffice
{
    public partial class TextField : System.Web.DynamicData.FieldTemplateUserControl
    {


        public override string FieldValueString
        {
            get
            {
                var MAX_DISPLAYLENGTH_IN_LIST = 140;
                if (this.Table.ToString() == "UserTransactions")
                    MAX_DISPLAYLENGTH_IN_LIST = 40;

                string value = base.FieldValueString;
                if (ContainerType == ContainerType.List)
                {
                    if (value != null && value.Length > MAX_DISPLAYLENGTH_IN_LIST)
                    {
                        value = value.Substring(0, MAX_DISPLAYLENGTH_IN_LIST - 3) + "...";
                    }
                }
                return value;
            }
        }

        public override Control DataControl
        {
            get
            {
                return Literal1;
            }
        }

    }
}
