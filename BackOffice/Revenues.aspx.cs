﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BackOffice
{
    public partial class Revenues : System.Web.UI.Page
    {
        private String DATE_FORMAT = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;

        public string DateFrom
        {
            get { return txbDateFrom.Text; }
        }

        public string DateTo
        {
            get { return txbDateTo.Text; }
        }

  

        protected void Page_Init(object sender, EventArgs e)
        {
            // set correct date time format
            txbDateFrom_CalendarExtender.Format = DATE_FORMAT;
            txbDateTo_CalendarExtender.Format = DATE_FORMAT;


        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRangeButton_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            if (button.ID == btnClear.ID)
            {
                txbDateFrom.Text = String.Empty;
                txbDateTo.Text = String.Empty;
                Label1.Text = string.Empty;
            }
            else
            {
                var from = DateTime.Parse(txbDateFrom.Text);
                var to = DateTime.Parse(txbDateTo.Text);
                using (var context = new Common.cellEntities())
                {
                    var dateCharge = context.UserTransactions.Where(a => a.TransactionDate >= from && a.TransactionDate < to && a.State == Common.Utility.DELIVRD).Sum(s => s.Tariff);
                    Label1.Text = dateCharge.HasValue ? dateCharge.ToString() : "0.00";


                    
                }
            }
            
        }
    }
}