﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigrationApp
{
    public class User
    {
        public string PhoneNumber { get; set; }
        public string Id { get; set; }
        public string ServiceId { get; set; }
        public string Status { get; set; }
        public string CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Balance { get; set; }
        public string SentCount { get; set; }
        public string AffiliateID { get; set; }
        public string IP { get; set; }
        public string PinCode { get; set; }
        public string DeleteMethod { get; set; }
        public string JoinMethod { get; set; }
        public string Operator { get; set; }
        public string AffiliateParams { get; set; }
        public string URL { get; set; }
    }
}
